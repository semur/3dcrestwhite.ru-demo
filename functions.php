<?php //include_once 'FT/FT_scope.php'; FT_scope::init(); ?>
<?php

if (!function_exists('setup')){
    function setup(){ 
        
        //enable theme thumbnails
        add_theme_support('post-thumbnails'); 
        
        //hook woocommerse into theme
        add_theme_support( 'woocommerce' );

        register_nav_menus( array(
		  'primary' => __( 'Primary Menu', '3dcrestwhite' ), 
		));
		
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		)); 
    }
}  
add_action('after_setup_theme', 'setup');
   

// ///////////////scripts and styles and other header stuff/////////////////
include(get_template_directory().'/functions/scripts-styles.php'); 

// // ///////////////HELPER FUNCTIONS/////////////////
include(get_template_directory().'/functions/head-includes.php');  
include(get_template_directory().'/functions/helper-functions.php'); 
include(get_template_directory().'/functions/content-types.php');  
include(get_template_directory().'/functions/placeholders.php');   
include(get_template_directory().'/functions/walkers.php');   
   

// /////////////////////////////WP API ENDPOINTS////////////////////////// 
include(get_template_directory().'/api/init.php'); 
include(get_template_directory().'/api/routes.php'); 
include(get_template_directory().'/api/products.php'); 
include(get_template_directory().'/api/email.php'); 
include(get_template_directory().'/api/instagram.php'); 
include(get_template_directory().'/api/customers.php'); 
include(get_template_directory().'/api/cart.php'); 
include(get_template_directory().'/api/info.php');  
include(get_template_directory().'/api/other.php');   