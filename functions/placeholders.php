<?php


/*
Register custom menu items
*/
if( function_exists('acf_add_options_page') ) {
    
  // add parent
  $parent = acf_add_options_page(array(
      'page_title'    => 'Global Settings',
      'menu_title'    => 'Global Settings',
      'redirect'      => true
  ));
   
  // add sub page
  acf_add_options_sub_page(array(

      'page_title'  => 'Text Placeholders',
      'menu_title'  => 'Globals',
      'menu_slug'   => 'theme-general-settings-global',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));
  // add sub page for whitestrips
  acf_add_options_sub_page(array(

      'page_title'  => 'Product Attributes',
      'menu_title'  => 'Attributes (W)',
      'menu_slug'   => 'theme-general-settings-products-w',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));
  // add sub page for toothpaste
  acf_add_options_sub_page(array(

      'page_title'  => 'Product Attributes',
      'menu_title'  => 'Attributes (T)',
      'menu_slug'   => 'theme-general-settings-products-t',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));
  // add sub page for toothpaste
  acf_add_options_sub_page(array(

      'page_title'  => 'Instructions',
      'menu_title'  => 'Instructions',
      'menu_slug'   => 'theme-general-settings-instructions',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));
  // add sub page for toothpaste
  acf_add_options_sub_page(array(

      'page_title'  => 'Instagram',
      'menu_title'  => 'Instagram',
      'menu_slug'   => 'theme-general-settings-instagram',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));

  // add sub page for toothpaste
  acf_add_options_sub_page(array(

      'page_title'  => 'Integration',
      'menu_title'  => 'Integration',
      'menu_slug'   => 'theme-general-settings-integration',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));

  // add sub page for toothpaste
  acf_add_options_sub_page(array(

      'page_title'  => 'Prompt',
      'menu_title'  => 'Prompt',
      'menu_slug'   => 'theme-general-settings-prompt',
      'capability'  => 'edit_posts',
      'icon_url' => 'dashicons-images-alt2',
      'position' => false, 
      'redirect'    => false, 
      'parent_slug'   => $parent['menu_slug'],
  ));
} 
 



function get_woo_categories($sub_levels = false) {

    $taxonomy     = 'product_cat';
    $orderby      = 'name';  
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title        = '';  
    $empty        = 0;

    $args = array(
        'taxonomy'     => $taxonomy,
        'orderby'      => $orderby,
        'show_count'   => $show_count,
        'pad_counts'   => $pad_counts,
        'hierarchical' => $hierarchical,
        'title_li'     => $title,
        'hide_empty'   => $empty
    );

    $categories = array();

    $all_categories = get_categories( $args ); 
    foreach ($all_categories as $cat) { 
        if($cat->category_parent == 0) {
            $category_id = $cat->term_id;
            
            $temp['name'] = $cat->name;
            $temp['slug'] = urldecode($cat->slug);
            $temp['url']  = urldecode(get_term_link($cat->slug, 'product_cat'));
            array_push($categories, $temp);
            

            if($sub_levels){
                $args2 = array(
                  'taxonomy'     => $taxonomy,
                  'child_of'     => 0,
                  'parent'       => $category_id,
                  'orderby'      => $orderby,
                  'show_count'   => $show_count,
                  'pad_counts'   => $pad_counts,
                  'hierarchical' => $hierarchical,
                  'title_li'     => $title,
                  'hide_empty'   => $empty
                );
                $sub_cats = get_categories( $args2 );
                if($sub_cats) {
                    foreach($sub_cats as $sub_category) {
                        echo  $sub_category->name ;
                    }  
                }  
            }      
        }     
    }  
    return $categories;
}
add_action( 'init', 'get_categories' );


/*
Add custom # of products displayed
*/
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 200 );

 
function shortcode_button_script() {

    if(wp_script_is("quicktags")){ 

        $arr = array("price-flex","price-glam","price-prof","price-1hr","price-gentle","price-vivid","price-sensi");

        foreach ($arr as $key => $value) { 
        ?>
            <script type="text/javascript"> 
   
                QTags.addButton( 
                    "code_shortcode<?php echo $key; ?>", 
                    "<?php echo $value; ?>", 
                    function(){
                        var txtarea = document.getElementById("content");
                        var start = txtarea.selectionStart;
                        var finish = txtarea.selectionEnd;  
                        var selected_text = txtarea.value.substring(start, finish); 
                        QTags.insertContent("[<?php echo $value; ?>]" +  selected_text + "[/<?php echo $value; ?>]");
                    }
                ); 
            </script>
        <?php
        }
    }
}   
add_action("admin_print_footer_scripts", "shortcode_button_script"); 

 
function get_shortcode_price($key){
 
    $pairs = array(
      "price-flex" => 245, 
      "price-glam" => 248, 
      "price-prof" => 239, 
      "price-1hr" => 533, 
      "price-gentle" => 557, 
      "price-vivid" => 552, 
      "price-sensi" => 285
    );

    $regular_price = get_post_meta( $pairs[$key], '_regular_price');
    $sale_price = get_post_meta( $pairs[$key], '_sale_price');
    $price = "";
    if($sale_price[0] != '') 
        $price = $sale_price[0];
    else
        $price = $regular_price[0];   
    return $price;
}

function custom_shortcode_flex() {
  return get_shortcode_price("price-flex");
}
add_shortcode( 'price-flex', 'custom_shortcode_flex' );

function custom_shortcode_glam() {
  return get_shortcode_price("price-glam");
}
add_shortcode( 'price-glam', 'custom_shortcode_glam' );

function custom_shortcode_prof() {
  return get_shortcode_price("price-prof");
}
add_shortcode( 'price-prof', 'custom_shortcode_prof' );

function custom_shortcode_1hr() {
  return get_shortcode_price("price-1hr");
}
add_shortcode( 'price-1hr', 'custom_shortcode_1hr' );

function custom_shortcode_gentle() {
  return get_shortcode_price("price-gentle");
}
add_shortcode( 'price-gentle', 'custom_shortcode_gentle' );

function custom_shortcode_vivid() {
  return get_shortcode_price("price-vivid");
}
add_shortcode( 'price-vivid', 'custom_shortcode_vivid' );

function custom_shortcode_sensi() {
  return get_shortcode_price("price-sensi");
}
add_shortcode( 'price-sensi', 'custom_shortcode_sensi' );