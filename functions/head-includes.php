<?php
/*
Global settings used throughout the site
*/
add_action('wp_head', 'settings', 6);
function settings(){ 
 
	$social = get_field("global_instagram_link","options"); 
	$timeoutCart = get_field("global_cart_timeout","options")*1000;

	$api = new products(); 
	$api->main();
	$pickForm = addslashes(json_encode($api->response->data));


    $api = new instagramLive();  
    $api->getAll();   
	$instagramReviews = addslashes(json_encode($api->response->data));

 	?>
	<script>
		settings = {
			instagram : "<?php echo $social; ?>",
		    cartTimeout : "<?php echo $timeoutCart; ?>",
			url : "/wp-content/themes/3dcrestwhite",
		    animationTime : 1000,
		    dbug : false,
		    infoDefault : '<?php echo get_field("global_info_messages_enable","options"); ?>',
		    devices : { 
		    	iPad: 800 
		    },
	 		shakerInterval: 10000,
			pickFormImage: '<?php echo wp_get_attachment_image(1180,"full",false,array("class"=>"pick-form-image", "id"=>"form-image", "width"=>"2000")); ?>',
			staticData: {
				pickForm: JSON.parse('<?php echo $pickForm; ?>'),
				instagramReviews: JSON.parse('<?php echo $instagramReviews; ?>'),
			}
		}
	</script>


	<?php
            
}