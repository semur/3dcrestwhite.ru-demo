<?php
 
class helpers {

    function timeElapsed($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'год',
            'm' => 'месяц',
            'w' => 'неделя',
            'd' => 'день',
            'h' => 'час',
            'i' => 'минута',
            's' => 'секунда',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 'ев' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' назад' : 'недавно';
    }


    function getVideo( $value) {
        
        $url = ''; 
        if(trim($value['product_video_embed']) !=''){
             
            $url .= $value['product_video_embed'];  

        }else{
             
            $url .= '<video width="320" height="240" autoplay="">';
            $url .= '  <source src="'.$value['product_video_mp4'].'" type="video/mp4">';
            $url .= '  <source src="'.$value['product_video_webm'].'" type="video/ogg">';
            $url .= '  <source src="'.$value['product_video_ogv'].'" type="video/webm">';
            $url .= '  Your browser does not support the video tag.';
            $url .= '</video>';   
        } 
        return $url;  
    } 

    function theVideo( $video ) {
        echo getVideo( $video );
    }

    function getDateRU($id){
        $monthes = array(
            1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
            5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
            9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
        );  
        return get_the_date('d',$id) .' '. $monthes[(get_the_date('n',$id))] .' '. get_the_date('Y',$id);
    } 

    function getImage( $size='large', $id="" ) {
        global $post;
        if($id=="")
            $id = $post->ID; 

        $img = wp_get_attachment_image_src( get_post_thumbnail_id($id),$size );

        return $img[0];
    }
    function theImage( $size='large', $id="" ) {
        global $post;
        if($id=="")
            $id = $post->ID;
        echo self::getImage( $size,$id ) ;
    }



    function isJSON($str) {

        @json_decode($str);
        return json_last_error() == JSON_ERROR_NONE;
    }

    function tagFilter($str) {

        if(!$this->isJSON($str)){

            $str = json_encode($str);  
            $str = str_replace("#", " #", $str); 
            $str = json_decode($str);

        }else{

            $str = str_replace("#", " #", $str); 
        }
        return $str;
    }

    function cutCharacters($text, $limit=20){ 

        $after = '';
        if(strlen($text) > $limit)
            $after = '...'; 
        $text = mb_substr($text, 0, $limit,'UTF-8').$after;

        return $text;  
    }
    function cutWords($text, $limit=20) {
     
        $text = strip_tags($text);
        $text = explode(" ", $text );
        $count = count($text);
        $after = '';
        if($count > $limit)
            $after = '...'; 
        $temp = '';
        $c = 0;
        foreach ($text as $key => $value) {
            $temp = $temp.' '.$value;
            if($c==$limit)
                break;
            $c++;
        }
        $text = $temp.$after;
        return $text; 
    }


    function get_compare($id, $flag='star'){
        $arr = array();

        if($flag=="star")
            $icon = '<i class="fa fa-star"></i>';
        else if($flag=="check")
            $icon = '<i class="fa fa-check" aria-hidden="true"></i>';

        $category = get_the_terms( $id,'product_cat')[0]->term_id;
        if($category==7) 
            $attributes = get_field("product_compare_toothpaste_attributes",$id);
        else if($category==8)
            $attributes = get_field("product_compare_whitestrips_attributes",$id);

        if(is_array($attributes))
            foreach ($attributes as $value) { 
                  
                $star = $value["product_compare_whitestrips_attributes_attribute_star"]; 
                $text = $value["product_compare_whitestrips_attributes_attribute_text"]; 
      
                if($star)  
                    array_push($arr, $icon);
                else if($text=="" && $flag=="check")
                    array_push($arr, '<i class="fa fa-times" aria-hidden="true"></i>');
                else
                    array_push($arr, $text);
            }
        return $arr;
    }
    function do_compare($id, $flag='star'){

        if($flag=="star")
            $icon = '<i class="fa fa-star"></i>';
        else if($flag=="check")
            $icon = '<i class="fa fa-check" aria-hidden="true"></i>';

        $category = get_the_terms( $id,'product_cat')[0]->term_id;
        if($category==7) 
            $attributes = get_field("product_compare_toothpaste_attributes",$id);
        else if($category==8)
            $attributes = get_field("product_compare_whitestrips_attributes",$id);

        foreach ($attributes as $value) { 
        echo '<div class="ranks">';
             
            $star = $value["product_compare_whitestrips_attributes_attribute_star"]; 
            $text = $value["product_compare_whitestrips_attributes_attribute_text"]; 
            if($star)
                echo $icon;
            else if($text=="" && $flag=="check")
                echo '<i class="fa fa-times" aria-hidden="true"></i>';
            else  
                echo $text;
             
        echo '</div>';
        }
    }

    function get_short_title($product){

        $title = get_the_title($product->ID); 
        if(get_field('product_short_title',$product->ID)!="")
            $title = get_field('product_short_title',$product->ID); 
        return $title; 
    }
    function the_short_title($product){
        echo get_short_title($product);
    }
}