<?php

function create_post_type() { 

  // register_post_type( 'faq',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Faq Tables' ),
  //       'singular_name' => __( 'Faq' )
  //     ),
  //     'public' => true, 
  //     'has_archive' => false,
  //     'rewrite' => array( 'slug' => 'faq' ),
  //     'has_single' => true, 
  //     'exclude_from_search' => true,
  //     'menu_icon' =>'dashicons-editor-help',
  //     'supports' => array( 'title' ),
  //   )
  // ); 

  // register_post_type( 'faq-posts',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'FAQ Posts' ),
  //       'singular_name' => __( 'FAQ' )
  //     ),
  //     'public' => true, 
  //     'has_archive' => true, 
  //     'has_single' => true, 
  //     'rewrite' => array( 'slug' => 'часто-задаваемые-вопросы' ),
  //     'exclude_from_search' => false,
  //     'menu_icon' =>'dashicons-editor-help',
  //     'supports' => array( 'title','editor','excerpt' ),
  //   )
  // ); 

  // register_post_type( 'reviews',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Reviews' ),
  //       'singular_name' => __( 'Review' )
  //     ),
  //     'public' => true,    
  //     'has_archive' => true, 
  //     'has_single' => true, 
  //     'rewrite' => array( 'slug' => 'отзывы-на-отбеливающие-полоски' ),
  //     'exclude_from_search' => true,
  //     'menu_icon' =>'dashicons-clipboard',
  //     'supports' => array( 'excerpt','title' ),
  //   )
  // ); 

  // register_post_type( 'instagram-photos',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Instagram Photos' ),
  //       'singular_name' => __( 'Instagram Photo' )
  //     ),
  //     'public' => true, 
  //     'has_archive' => true, 
  //     'has_single' => false,  
  //     'rewrite' => array( 'slug' => 'instagram' ),
  //     'exclude_from_search' => true,
  //     'menu_icon' =>'dashicons-images-alt',
  //     'supports' => array( 'thumbnail' ),
  //   )
  // ); 

  // register_post_type( 'instagram-feed',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Instagram Feed' ),
  //       'singular_name' => __( 'Instagram Feed' )
  //     ),
  //     'public' => true, 
  //     'has_archive' => false, 
  //     'has_single' => false,  
  //     'rewrite' => array( 'slug' => 'instagram-feed' ),
  //     'exclude_from_search' => true,
  //     'menu_icon' =>'dashicons-images-alt',
  //     'supports' => array( 'title','excerpt' ),
  //   )
  // ); 

  // register_post_type( 'customers',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Customers' ),
  //       'singular_name' => __( 'Customer' )
  //     ),
  //     'public' => true, 
  //     'has_archive' => false, 
  //     'has_single' => false,   
  //     'exclude_from_search' => true,
  //     'menu_icon' =>'dashicons-images-alt',
  //     'supports' => array( 'title','excerpt' ),
  //   )
  // ); 

  // register_post_type( 'callbacks',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Callbacks' ),
  //       'singular_name' => __( 'Callback' )
  //     ),
  //     'public' => true, 
  //     'has_archive' => false, 
  //     'has_single' => false,   
  //     'exclude_from_search' => true,
  //     'menu_icon' =>'dashicons-images-alt',
  //     'supports' => array( 'title' ), //titles will be dates in this case
  //   )
  // ); 
 
  flush_rewrite_rules();    
}
// add_action( 'init', 'create_post_type' );
 


// add_action( 'init', 'my_custom_post_type_rest_support', 25 );

// function my_custom_post_type_rest_support_helper( $post_type_name ) {
//   global $wp_post_types;
 
//   if( isset( $wp_post_types[ $post_type_name ] ) ) {
//       $wp_post_types[$post_type_name]->show_in_rest = true;
//       $wp_post_types[$post_type_name]->rest_base = $post_type_name;
//       $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
//   }

// }

// function my_custom_post_type_rest_support(){

//   my_custom_post_type_rest_support_helper('instagram-feed');
//   my_custom_post_type_rest_support_helper('instagram-photos');
//   my_custom_post_type_rest_support_helper('faq');
//   my_custom_post_type_rest_support_helper('slides');
//   my_custom_post_type_rest_support_helper('faq-posts'); 
//   my_custom_post_type_rest_support_helper('reviews');   
// }
?>