<?php

class WalkerAngular extends Walker_Nav_Menu{

  private $prev_id = NULL;
  private $submenu_ids = array();


  /*
  Search in given array, return boolean
  */
  function exists($arr,$target){

    for ($i=0;$i<count($arr);$i++) {
      $value = $arr[$i];
      if($value==$target)
        return TRUE;  
    }
    return FALSE;
  }

  /*
  Displays start of a level. E.g '<ul>'
    @see Walker::start_lvl()
  */
    function start_lvl(&$output, $depth=0, $args=array()) {
   
      if($this->exists($this->submenu_ids, $this->prev_id))
        $angular = 'ng-show="navigationDropdown('.$this->prev_id.')" ng-cloak';

      //$this->pre_id
        $output .= "\n<ul $angular class='sub-menu'>\n";
    }

    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array|object $args    Additional strings. Actually always an 
                                     instance of stdClass. But this is WordPress.
     * @return void
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
      
      $this->prev_id = $item->ID;

        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

         
        $attributes  = ''; 
        if(!empty( $item->attr_title ))
            $attributes .= ' title="'.esc_attr( $item->attr_title ).'"';

        $parent = FALSE;  
        foreach($classes as $key=>$val){
          
          if($val=="menu-item-has-children")
            $parent = TRUE;
        }
        $fa = "";
        if($classes[0]=="fa"){
          $fa = '<i class="'.$classes[0].' '.$classes[1].'"></i>';
          array_shift($classes);
          array_shift($classes);
        } 

        $class_names = join(
            ' '
        ,   apply_filters(
                'nav_menu_css_class'
            ,   array_filter( $classes ), $item
            )
        );

        if(!empty($class_names))
          $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= "<li id='menu-item-$item->ID' $class_names>";   
        $title = apply_filters( 'the_title', $item->title, $item->ID );
 
        if(!$parent){

          if(!empty( $item->target ))
              $attributes .= ' target="'.esc_attr( $item->target).'"';
          if(!empty( $item->xfn ))
              $attributes .= ' rel="'.esc_attr( $item->xfn).'"';

          if(!empty( $item->url ))
              $attributes .= ' href="'.esc_attr( $item->url).'"';
        }

        if($depth==0 && $parent){ 
          $angular = '';
          array_push($this->submenu_ids, $item->ID);

          $item_output = $args->before
                      . "<span class='link' $attributes ng-click='navigationTrigger(".$item->ID.")' ng-class='navigationDropdownIsActive(".$item->ID.")'>"
                      . $args->link_before
                      . $title
                      . '</span> '
                      . $args->link_after 
                      . $args->after;
        }else if($depth!=0 && !$parent){
 
          $item_output = $args->before
                        . "<a class='link' $attributes>"
                        . $fa
                        . $args->link_before
                        . $title
                        . '</a> '
                        . $args->link_after 
                        . $args->after;
        }else{
 
          $item_output = $args->before
                        . "<a class='link' $attributes>" 
                        . $args->link_before
                        . $title
                        . '</a> '
                        . $args->link_after 
                        . $args->after;
        }

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el',
            $item_output,
            $item,
            $depth,
            $args
        );
    }
}


 
class NoClassWalker extends Walker_Nav_Menu{

    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array|object $args    Additional strings. Actually always an 
                                     instance of stdClass. But this is WordPress.
     * @return void
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
    
        $classes = empty ( $item->classes ) ? array () : (array) $item->classes;
 
        $output .= "<li>";

        $attributes  = ''; 
        if(!empty( $item->attr_title ))
            $attributes .= ' title="'.esc_attr( $item->attr_title ).'"';
        if(!empty( $item->target ))
            $attributes .= ' target="'.esc_attr( $item->target).'"';
        if(!empty( $item->xfn ))
            $attributes .= ' rel="'.esc_attr( $item->xfn).'"';
        if(!empty( $item->url ))
            $attributes .= ' href="'.esc_attr( $item->url).'"';
 
        $title = apply_filters( 'the_title', $item->title, $item->ID );

        $item_output = $args->before
                      . "<a $attributes>"
                      . $args->link_before
                      . $title
                      . '</a> '
                      . $args->link_after 
                      . $args->after;

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el',
            $item_output,
            $item,
            $depth,
            $args
        );
    }
}



class MobileWalker extends Walker_Nav_Menu{

    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array|object $args    Additional strings. Actually always an 
                                     instance of stdClass. But this is WordPress.
     * @return void
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
    
        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

         
        $attributes  = ''; 
        if(!empty( $item->attr_title ))
            $attributes .= ' title="'.esc_attr( $item->attr_title ).'"';

        $parent = FALSE;  
        foreach($classes as $key=>$val){
          
          if($val=="menu-item-has-children")
            $parent = TRUE;
        }
        $fa = "";
        if($classes[0]=="fa"){
          $fa = '<i class="'.$classes[0].' '.$classes[1].'"></i>';
          array_shift($classes);
          array_shift($classes);
        }  

        $output .= "<li>";   
        $attributes  = ''; 
        if(!empty( $item->attr_title ))
            $attributes .= ' title="'.esc_attr( $item->attr_title ).'"';

        $parent = FALSE;
        if(property_exists ($item,"classes")){
          foreach($item->classes as $key=>$val){
            
            if($val=="menu-item-has-children")
              $parent = TRUE;
          }
        }
        $title = apply_filters( 'the_title', $item->title, $item->ID );

        if(!$parent){

          if(!empty( $item->target ))
              $attributes .= ' target="'.esc_attr( $item->target).'"';
          if(!empty( $item->xfn ))
              $attributes .= ' rel="'.esc_attr( $item->xfn).'"';
          if(!empty( $item->url ))
              $attributes .= ' href="'.esc_attr( $item->url).'"';
        }
  
        if($depth==0 && $parent){  

          $item_output = $args->before
                      . "<span class='link' $attributes>"
                      . $fa
                      . $args->link_before 
                      . $title
                      . '</span> '
                      . $args->link_after 
                      . $args->after;
        }else{
 
          $item_output  = $args->before
                        . "<a class='link' $attributes>"
                        . $fa
                        . $args->link_before
                        . $title
                        . '</a> '
                        . $args->link_after 
                        . $args->after;
        }

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el',
            $item_output,
            $item,
            $depth,
            $args
        );
    }
}