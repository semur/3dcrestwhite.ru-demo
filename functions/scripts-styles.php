<?php


/**
 * Enqueue scripts and styles
 */
function scripts() {
  	

	//jQuery
	wp_enqueue_script( 'jquery-js', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js');
 	 

  	if(WP_LOCAL){
	  	
	  	//fontawesome
	 	wp_enqueue_style( 'fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		//Angular
		wp_enqueue_script( 'angular-angular-js', get_template_directory_uri() . '/public/js/angular.min.js');  
		// wp_enqueue_script( 'angular-angular-js', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js');  

		wp_enqueue_script( 'angular-cookies-js', get_template_directory_uri() . '/public/js/angular-cookies.min.js'); 
		wp_enqueue_script( 'angular-masonry-js', get_template_directory_uri() . '/public/js/angular-masonry.min.js'); 
 	
 		//horizontal scroll
		wp_enqueue_script( 'angular-sly-plugin-js', get_template_directory_uri() . '/public/js/jquery.sly.plugins.js'); 
		wp_enqueue_script( 'angular-sly-js', get_template_directory_uri() . '/public/js/jquery.sly.min.js'); 

 		//masonry  
	  	wp_enqueue_script( 'imagesloaded-js', get_template_directory_uri() . '/public/js/jquery.imagesloaded.min.js' );
	  	wp_enqueue_script( 'masonry-js', get_template_directory_uri() . '/public/js/jquery.masonry.js' );  
	  
		// sticky elements
		wp_enqueue_script( 'sticky-js', get_template_directory_uri() . '/public/js/jquery.sticky.js' );  

		// mmenu
		wp_enqueue_style( 'jquery.mmenu.all-css', get_template_directory_uri() . '/public/css/jquery.mmenu.all.css');
		wp_enqueue_script( 'jquery.mmenu.min.all-js', get_template_directory_uri() . '/public/js/jquery.mmenu.all.min.js' );  
	 
		//core styles
		wp_enqueue_style( 'reset-css', get_template_directory_uri() . '/public/css/reset.css');  
		wp_enqueue_style( 'imports-css', get_template_directory_uri() . '/public/css/imports.css');  
		wp_enqueue_style( 'style-css', get_template_directory_uri() . '/public/css/styles.css');  
		wp_enqueue_style( 'media-css', get_template_directory_uri() . '/public/css/media.css'); 

	 	//core scripts
		wp_enqueue_script( 'scripts', get_template_directory_uri() . '/public/js/scripts.js');

		//core angular scripts
		wp_enqueue_script( 'app', get_template_directory_uri() . '/public/js/app.js');
		wp_enqueue_script( 'directives', get_template_directory_uri() . '/public/js/directives.js');
		wp_enqueue_script( 'helpers', get_template_directory_uri() . '/public/js/helpers.js');
		wp_enqueue_script( 'controllers', get_template_directory_uri() . '/public/js/controllers.js');
	 
  	}else{ 
   	
		$date = new DateTime(); 
		wp_enqueue_script( 'min', get_template_directory_uri() . '/public/js/min.js',array("jquery-js"), $date->getTimestamp()."", true); 
		
		//this is defined in header.php
	 	wp_enqueue_style( 'fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
		wp_enqueue_style( 'min-css', get_template_directory_uri() . '/public/css/min.css',102,1.1);  

  	}

	 
   
}
add_action( 'wp_enqueue_scripts', 'scripts' ); 




function scripts_rewrite(){
	global $wp_scripts, $wp_styles;
  	
  	// print_r($wp_styles);
  	
	/* deregister css */
	wp_deregister_style("contact-form-7");
	wp_deregister_style("woocommerce-layout");
	wp_deregister_style("woocommerce-smallscreen");
	wp_deregister_style("woocommerce_prettyPhoto_css"); 
	wp_deregister_style("woocommerce-general"); 

	/* deregister js */ 
	if(!is_page(684)){
		
		wp_deregister_script("jquery-form");
		wp_deregister_script("jquery-form-7");
		wp_deregister_script("wc-cart-fragments");
		wp_deregister_script("wc-credit-card-form");
		wp_deregister_script("wc-add-to-cart-variation");
		wp_deregister_script("wc-single-product");
		wp_deregister_script("wc-country-select");
		wp_deregister_script("wc-address-i18n");
		wp_deregister_script("wc-password-strength-meter");
		wp_deregister_script("wc-add-to-cart");
		wp_deregister_script("woocommerce");
		wp_deregister_script("wc-cart-fragments");
		
		if(!is_admin() && !is_user_logged_in()){
		
			wp_deregister_script("jquery");
			wp_deregister_script("jquery-core");
			wp_deregister_script("jquery-migrate");
			wp_deregister_script("jquery-ui-core"); 
			wp_deregister_script("wp-embed");  
		}
	} 
}
add_action( 'wp_enqueue_scripts', 'scripts_rewrite',999999999); 	 