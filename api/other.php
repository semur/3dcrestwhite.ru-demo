<?php
/*
##################################################################
Classes for other
##################################################################
*/
class other {
   	
   	/*
	Constructor for defining globals
	*/
	var $response = array(); 

	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	} 

 	function getCelebrities(){
 	
 		$arr = array();
		$images = get_field("prompt_images","options");
		foreach ($images as $key => $value) : 
			
			$value = $value["prompt_image"]["sizes"]["shop_catalog"]; 

			$arr[] = (object)array(
				"url" => get_field("global_instagram_link","options"),
				"img" => $value,
			); 
		endforeach; 

   		$this->response->success = TRUE;   		
   		$this->response->data = $arr; 
 	}  
}