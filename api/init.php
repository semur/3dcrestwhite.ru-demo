<?php
add_action( 'init', 'register_content_types' );


/**
 * Register post types
 */
function register_content_types() {
    
    /**
     * Register an instagram review post type. 
     */    
    register_post_type( 'instagram-feed-l', array(
        'labels'             => array(
                                'name'               => _x( 'Feed (L)', 'post type general name', 'your-plugin-textdomain' ), 
                            ),
        'description'        => __( 'Instagram Feed - live results from our instagram page', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'instagram-feed-live' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-images-alt',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
    ));

    /**
     * Register an instagram review post type. 
     */    
    register_post_type( 'instagram-feed-s', array(
        'labels'             => array(
                                'name'               => _x( 'Feed (S)', 'post type general name', 'your-plugin-textdomain' ), 
                            ),
        'description'        => __( 'Instagram Feed - custom results from our instagram page', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'instagram-feed-static' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-images-alt2',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
    ));
  
    /**
     * Register a product review post type to be linked to store products. 
     */
    register_post_type( 'reviews', array(
        'labels'             => array(
                                'name'               => _x( 'Reviews', 'post type general name', 'your-plugin-textdomain' ), 
                            ),
        'description'        => __( 'Reviews', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'reviews' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-menu',
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    ));


    /**
     * Register a customer post type. 
     */
    register_post_type( 'customers', array(
        'labels'             => array(
                                'name'               => _x( 'Customers', 'post type general name', 'your-plugin-textdomain' ), 
                            ),
        'description'        => __( 'Customers', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'customers' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-admin-users',
        'supports'           => array( 'title', )
    ));


    /**
     * Register a callback post type. 
     */
    register_post_type( 'callbacks', array(
        'labels'             => array(
                                'name'               => _x( 'Callbacks', 'post type general name', 'your-plugin-textdomain' ), 
                            ),
        'description'        => __( 'Callbacks', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'callbacks' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-controls-repeat',
        'supports'           => array( 'title' )
    ));

    /**
     * Register a FAQ post type. 
     */
    register_post_type( 'faq-posts', array(
        'labels'             => array(
                                'name'               => _x( 'FAQs', 'post type general name', 'your-plugin-textdomain' ), 
                            ),
        'description'        => __( 'FAQs', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'callbacks' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-welcome-learn-more',
        'supports'           => array( 'title', 'editor', 'tags') ,
        'taxonomies'         => array( 'category' ),
    ));
}
 

/**
* Add REST API support to an already registered taxonomy.
*/    
add_action( 'init', 'my_custom_post_type_rest_support', 25 );
function my_custom_post_type_rest_support() {
    global $wp_post_types;

    /*
    API route for products
    */
    $post_type_name = 'product';
    if( isset( $wp_post_types[ $post_type_name ] ) ) {
        $wp_post_types[$post_type_name]->show_in_rest = true;
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
    }
}
 


function sb_add_cpts_to_api( $args, $post_type ) {
    if ( 'movie' === $post_type ) {
        $args['show_in_rest'] = true;
        $args['rest_base'] = 'movie';
        // $args['rest_controller_class'] = 'WP_REST_Posts_Controller';
    }
    return $args;
}
add_filter( 'register_post_type_args', 'sb_add_cpts_to_api', 10, 2 );
?>