<?php
/*
##################################################################
Classes for promotions
##################################################################
*/
class info {
   	
   	/*
	Constructor for defining globals
	*/
	var $response = array(); 

	/*
	Constructor for defining globals
	*/
	function __construct(){
		
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	}
  
	function getPromotion(){

		$this->response->data = (object)array(
			"status" => get_field("global_promotion_status","options"),//is it active right now
			"promotion" => get_field("global_promotion_text","options"),//text
		); 
		$this->response->success = TRUE; 
	}

	function getInfo($id){

		$dynamic = get_field("global_info_messages_dynamic","options"); 
		$found = FALSE;
		foreach ($dynamic as $key => $value) { 
			if($value["global_info_messages_page"]==$id){  
				$dynamic = (object)array( 
		 			"enable" => $value['global_info_messages_enable'],
		 			"timeout" => $value['global_info_messages_timeout'],
		 			"message" => $value['global_info_messages_message'], 
				); 
				$found = TRUE;
				break;
			}
		}    
		if($found==FALSE)
			$dynamic = NULL;

		$phone = get_field("global_phone","options");
		$default = (object)array(
 			"enable" => get_field("global_info_messages_enable","options"),
 			"timeout" => get_field("global_info_messages_timeout","options"),
 			"message" => get_field("global_info_messages_message","options").'<a href=\"tel:'.$phone.'\">'.$phone.'</a>',  
		);  

		$this->response->data = (object)array(
			"dynamic" => $dynamic,
			"default" => $default,
		);  
	}
}