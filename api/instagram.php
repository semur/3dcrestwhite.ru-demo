<?php
/*
##################################################################
Classes for the instagram reviews live and static 
##################################################################
*/
class instagramLive {
 
	/*
	Constructor for defining globals
	*/
	var $response = array();
	var $settings = array();

	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		); 

		$this->settings = (object)array(
			'apiKey'	  => get_field("instagram_reviews_live_settings_id","options"),
			'apiSecret'   => get_field("instagram_reviews_live_settings_secret","options"),
			'apiToken' 	  => get_field("instagram_reviews_live_settings_token","options"),
			'apiCallback' => "",   
		);   
	}
 
	function removeAll(){

		$query = new WP_Query(array(
	        'post_type' => 'instagram-feed-l', 
	        'posts_per_page' => -1,
	    ));
	   	  
	    if ($query->have_posts()) {
 			  
	        while ($query->have_posts()) {  
	        	$query->the_post(); 
	        	wp_delete_post(get_the_ID());
	        }
	    } 
	    $this->response->success = TRUE;
	    wp_reset_postdata();
	}

	/*
	Well not really all, just the first 10
	*/
	function getAll(){

		$query = new WP_Query(array(
	        'post_type' => 'instagram-feed-l', 
	        'posts_per_page' => 10,
	        'order' => 'DESC'
	    )); 
		$helpers = new helpers();

	   	$arr = array(); 	
	    if ($query->have_posts()) {
 			  
	        while ($query->have_posts()) {  
	        	$query->the_post(); 

	        	$arr[] = (object)array(
	        		'id'			 => get_the_ID(),
	        		'link'			 => get_field('instagram_reviews_live_link'),
	        		'likes_count' 	 => get_field('instagram_reviews_live_likes_count'), 
	        		'comments_count' => get_field('instagram_reviews_live_commenters_count'), 
	        		'image_small' 	 => get_field('instagram_reviews_live_image_small'),
					'image' 		 => get_field('instagram_reviews_live_image'),
	        		'caption'		 => $helpers->cutWords($helpers->tagFilter(get_field('instagram_reviews_live_caption'))),
	        		'date'			 => get_field('instagram_reviews_live_date'), 
	        	); 
	        }
	    } 
	    $this->response->success = TRUE;
	    $this->response->data = $arr;  
	    wp_reset_postdata();
	}

	function getOne($id){
 
		$query = new WP_Query(array(
	        'post_type' => 'instagram-feed-l', 
	        'posts_per_page' => 1,
	        'p' => $id
	    )); 
 		 
		//post exists  
		if ($query->have_posts()){ 
			$query->the_post(); 

			$helpers = new helpers(); 
		   	$response = (object)array(
				'id'			 => get_the_ID(),
				'link'			 => get_field('instagram_reviews_live_link',get_the_ID()),
				'likes_count' 	 => get_field('instagram_reviews_live_likes_count',get_the_ID()),
				'likers' 		 => (get_field('instagram_reviews_live_likers',get_the_ID())),
				'comments_count' => get_field('instagram_reviews_live_commenters_count'),
				'commenters'	 => ($helpers->tagFilter(get_field('instagram_reviews_live_comments',get_the_ID()))),
				'image' 		 => get_field('instagram_reviews_live_image',get_the_ID()),
				'image_small' 	 => get_field('instagram_reviews_live_image_small',get_the_ID()),
				'caption'		 => $helpers->cutWords($helpers->tagFilter(get_field('instagram_reviews_live_caption',get_the_ID()))),
				'date'			 => get_field('instagram_reviews_live_date',get_the_ID()),
				'tags'			 => (get_field('instagram_reviews_live_tags',get_the_ID())), 
			);
		    $this->response->success = TRUE;
		    $this->response->data = $response;  

		//if post doesn't exist
		}else   
		    $this->response->success = FALSE;

		wp_reset_postdata();
	}
  
	function updateAll(){
   
	    $url = 'https://api.instagram.com/v1/users/1959307945/media/recent/?access_token='.$this->settings->apiToken;

	    $result = wp_remote_get($url); 
	    
	    if($result!==undefined && $result['body']!==undefined)
	        $result = json_decode($result['body']);
	    if($result!==undefined && $result->data!==undefined)
	        $result = $result->data;   

	    $c = 0;
    	foreach ($result as $key => $item) {
	 		
	 		$p = get_page_by_title($item->created_time); 
	 		 
	 		$post_id = $p->ID; 
	 		if(is_array($p))
	 			$post_id = $p[0]->ID; 
 			$this->updateACF($item, $post_id);  
		} 

		$this->response->success = TRUE;
	}

	function insertAll(){
   
	    $url = 'https://api.instagram.com/v1/users/1959307945/media/recent/?access_token='.$this->settings->apiToken;

	    $result = wp_remote_get($url); 
	    
	    if($result!==undefined && $result['body']!==undefined)
	        $result = json_decode($result['body']);
	    if($result!==undefined && $result->data!==undefined)
	        $result = $result->data;   
 		
 		$limit = 10;
 		if($result!=null && is_array($result)){
	    	foreach ($result as $key => $item) {
		 		 
				$post_id = wp_insert_post (array (
					'post_type' 	=> 'instagram-feed-l',
					'post_title' 	=> $item->created_time, 
					'post_status' 	=> 'publish',
					'comment_status'=> 'closed',    
					'ping_status' 	=> 'closed',    
				));   
	 			$this->updateACF($item, $post_id); 

	 			if($limit < 0)
	 				break;
	 			$limit--;
			} 
			$this->response->success = TRUE;
			$this->response->data = FALSE;
		}else{
			$this->response->data = json_encode($result);
		}
	} 

	function updateACF($item, $post_id){ 
		update_field('instagram_reviews_live_link', $item->link, $post_id); 
		update_field('instagram_reviews_live_likes_count', $item->likes->count, $post_id); 
		update_field('instagram_reviews_live_likers', ($item->likes->data), $post_id); 
		update_field('instagram_reviews_live_commenters_count', $item->comments->count, $post_id); 
		update_field('instagram_reviews_live_comments', ($item->comments->data), $post_id); 
		update_field('instagram_reviews_live_image_small', $item->images->low_resolution->url, $post_id); 
		update_field('instagram_reviews_live_image', $item->images->standard_resolution->url, $post_id); 
		update_field('instagram_reviews_live_caption', $item->caption->text, $post_id); 
		
		update_field('instagram_reviews_live_date', $item->created_time, $post_id); 
		update_field('instagram_reviews_live_tags', ($item->tags), $post_id); 

		if(FALSE)
		print_r(array(
			$item->link,
			$item->likes->count,
			$item->likes->data,
			$item->comments->count,
			$item->comments->data,
			$item->images->standard_resolution->url,
			$item->caption->text,
			$item->created_time,
			$item->tags
		));
	}
}
 


class instagramStatic {
 
	var $response = array();
	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	}

	function getAll(){
 		
 		$hp = new helpers();
	    $query = new WP_Query(array(
	        'post_type' => 'instagram-feed-s', 
	        'posts_per_page' => -1,
	        'order' => array('type' => 'DESC', 'date' => 'DESC'), 
	    )); 
	   	$arr = array(); 

	    if ($query->have_posts()) {
	        while ($query->have_posts()) {
	        	$query->the_post();  

				$arr[get_the_ID()] = (object)array(
					"id" => get_the_ID(),
					"username" => get_the_title(),  
					"link" => get_field("instagram_reviews_static_link"), 
					"excerpt" => get_the_excerpt(),
					"img" => (object)array( 
						"src" 	=> $hp->getImage(get_the_ID()),
						"title" => "",
						"alt" 	=> "",
					)
				);   
			} 
		}   
	    $this->response->success = TRUE;
	    $this->response->data = $arr; 

	    wp_reset_postdata();  
 	} 
 	
 	function printAll(){
 
	  	echo '<pre>';
	  	print_r($this->getAll());
	  	echo '</pre>'; 
 	}  
}