<?php
/*
##################################################################
Classes for products
##################################################################
*/
class products {
   	
   	/*
	Constructor for defining globals
	*/
	var $response = array();
	var $settings = array();

	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	}

 	function getAll(){


	    $query = new WP_Query(array(
	        'post_type' => 'product', 
	        'posts_per_page' => -1,
	    ));
	   	
	   	$products = array();

	    if ($query->have_posts()) : 


			$factory = new WC_Product_Factory();   
			  
	        while ($query->have_posts()) : $query->the_post(); 
		            	  
				$regular_price = get_post_meta( get_the_ID(), '_regular_price');
				$sale_price = get_post_meta( get_the_ID(), '_sale_price');
	  	
	  			$category = get_the_terms( $post->ID, 'product_cat' )[0]; 

				$attributes = $factory->get_product(get_the_ID())->get_attributes(); 
				// get product by id
				$i=0; 
				$str = "";

				$arr = array();
				foreach ($attributes as $key => $value) : 
					  
					array_push($arr, (object)array(
						"key" => $value['name'],
						"value" => $value['value'],
					));
				endforeach; 
		  	
		  		$wp_product = get_post(get_the_ID());

				$products[get_the_ID()] = (object)array(
					"id" => get_the_ID(),
					"title" => get_the_title(),
					"short_title" => get_field("product_short_title",get_the_ID()),
					"price" => intval($regular_price[0]),
					"link" => get_the_permalink(),
					"category" => $category,
					"attributes" => $arr,
					"status" => $wp_product->post_status,
					"excerpt" => $wp_product->post_excerpt,
				); 
		 
	        endwhile; // end of the loop.   
		endif;  
		
		return $products;
 	}

 	function getOne($id){

	 	$products = $this->getAll();
 		if($products[$id]->status=="publish")
 			$product = $products[$id];
 		else
 			$product = NULL;

		return (object)$product;
 	}
 	
 	function printAll(){
 
	  	echo '<pre>';
	  	print_r($this->getAll());
	  	echo '</pre>'; 
 	}
 	function printOne($id){
 
	  	echo '<pre>';
	  	print_r($this->getOne($id));
	  	echo '</pre>'; 
 	}
 	

 	function main(){

 		$arrFast = array();
 		foreach (array($this->getOne(277), $this->getOne(533)) as $key => $value) { 
 			if($value!=null && $value->status=="publish")
 				array_push($arrFast, $value);  
 		}

 		$arrComfortable = array();
 		foreach (array($this->getOne(557), $this->getOne(552), $this->getOne(248)) as $key => $value) { 
 			if($value!=null && $value->status=="publish")
 				array_push($arrComfortable, $value);  
 		}
 
 		$arrResults = array();
 		foreach (array($this->getOne(426), $this->getOne(533), $this->getOne(245)) as $key => $value) { 
 			if($value!=null && $value->status=="publish")
 				array_push($arrResults, $value);  
 		}
 	
 		$this->response->success = TRUE;
 		$this->response->data = (object)array( 
	    	"fast" => $arrFast,
	    	"comfortable" => $arrComfortable,
	    	"results" => $arrResults,
	    	"relief" => array(
	    		$this->getOne(285),
	    	),
	    	"fortify" => array(
	    		$this->getOne(550),
	    	),
	    );
 	}  
}  