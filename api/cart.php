<?php
/*
##################################################################
Classes for the cars
##################################################################
*/
class cart {
   	
   	/*
	Constructor for defining globals
	*/
	var $response = array(); 

	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	}

	function getAll() {
	   	
	   	global $woocommerce;
		global $post;
		global $product;
	 
		$cart = WC()->instance()->cart;  
		$contents = array();
		$quantity = 0;
		foreach ( $cart->cart_contents as $key => $item ) { 
		 	$quantity += $item["quantity"];
	 
			$post = $item["data"]->post;
			$contents[] = (object)array(
				"quantity" => $item["quantity"],
				"total" => $item["line_total"],
				"id" => $post->ID,
				"name" => $post->post_title,
				"url" => get_permalink($post->id),
			);
		}
	 	
	 	$this->response->success = TRUE;
 		$this->response->data = (object)array(
	 		"contents" => $contents,
	    	"total" => $cart->cart_contents_total,
	    	"quantity" => $quantity
	    );
	}

	function removeAll(){
		global $woocommerce;
		global $post;
		global $product;

		foreach ( $woocommerce->cart->cart_contents as $cart_item_key => $cart_item ) {
			 
		 	$id = $woocommerce->cart->generate_cart_id($cart_item['product_id']);
		 	$cart_item_id = $woocommerce->cart->find_product_in_cart($id);
			$woocommerce->cart->set_quantity($cart_item_id,0);
		}
		$this->response->success = TRUE; 
	}

	function add($id, $quantity){
		global $woocommerce;
 		do_action( 'woocommerce_set_cart_cookies', TRUE );
 		
		for($i=0;$i<$quantity;$i++)
			$woocommerce->cart->add_to_cart($id);
		$this->response->success = TRUE;  
	}
}