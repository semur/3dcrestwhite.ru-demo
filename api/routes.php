<?php
 
add_action( 'rest_api_init', function () {


    /*
    Instagram
    */
    register_rest_route( 'instagram/v2', '/static/get/all', array(
        'methods' => 'GET',
        'callback' => function(){

        	$api = new instagramStatic();
        	$api->getAll();
		    return wp_send_json($api->response);
        },
    )); 
 
    register_rest_route( 'instagram/v2', '/live/get/all', array(
        'methods' => 'GET',
        'callback' => function(){ 

            $api = new instagramLive();   
              
            $api->getAll();  
            return wp_send_json($api->response);
        },
    )); 

    register_rest_route( 'instagram/v2', '/live/get/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => function($data){ 
              
            $api = new instagramLive();   
            $id = $data['id'];
            $api->getOne($id);  
            return wp_send_json($api->response);
        },
    )); 

    register_rest_route( 'instagram/v2', '/live/remove/all', array(
        'methods' => 'GET',
        'callback' => function(){ 
              
            $api = new instagramLive();   
            
            $api->removeAll();  

            return wp_send_json($api->response);
        },
    )); 
    register_rest_route( 'instagram/v2', '/live/update/all', array(
        'methods' => 'GET',
        'callback' => function(){ 
              
            $api = new instagramLive();   
              
            $api->updateAll(); 

            return wp_send_json($api->response);
        },
    )); 
    register_rest_route( 'instagram/v2', '/live/insert/all', array(
        'methods' => 'GET',
        'callback' => function(){ 
              
            $api = new instagramLive();   
              
            $api->insertAll(); 

            return wp_send_json($api->response);
        },
    )); 
 
    /*
    Products
    */
    register_rest_route( 'pick-form/v2', '/products', array(
        'methods' => 'GET',
        'callback' => function(){

            $api = new products(); 
        	$api->main();
        	wp_send_json($api->response);  
        },
    ));

    /*
    Customers
    */
    register_rest_route( 'customers/v2', '/callback/add', array(
        'methods' => 'GET',
        'callback' => function(){

            $api = new customers();
            $email = new emails();

            $message = (object)array(
                "phone" => $_GET['phone']
            );
 
            $api->callbackAdd($message); 
            $email->callbackEmail($message);
            wp_send_json($api->response); 
        },
    )); 

    register_rest_route( 'customers/v2', '/customer/add', array(
        'methods' => 'GET',
        'callback' => function(){ 
            $options = (object)array(
                'address' => $_GET['address'],
                'email' => $_GET['email'],
                'phone' => $_GET['phone'],
                'name' => $_GET['name'],
                'products' => $_GET['products'],
                'message' => $_GET['message'],
            ); 

            $api = new customers();
            $api->customerAdd($options);
            wp_send_json($api->response); 
        },
    ));  

    /*
    Cart
    */
    register_rest_route( 'cart/v2', '/get/all', array(
        'methods' => 'GET',
        'callback' => function(){

            $api = new cart();
            $api->getAll();

            return wp_send_json($api->response);
        },
    )); 
    register_rest_route( 'cart/v2', '/remove/all', array(
        'methods' => 'GET',
        'callback' => function(){

            $api = new cart(); 
            $api->removeAll();
            
            return wp_send_json($api->response);
        },
    ));
    register_rest_route( 'cart/v2', '/add/(?P<id>\d+)/(?P<quantity>\d+)', array(
        'methods' => 'GET',
        'callback' => function($data){

            $api = new cart();
            $id = $data['id'];
            $quantity = $data['quantity'];  
            $api->add($id, $quantity);

            return wp_send_json($api->response);
        },
    )); 

    /*
    Emails
    */ 
    register_rest_route( 'emails/v2', '/checkout/send', array(
        'methods' => 'GET',
        'callback' => function(){
  
            $email = new emails();    
            $message = (object)array(
                "message" => $email->buildCheckoutMessage($_GET['products']),
                "name" => $_GET['name'],
                "email" => $_GET['email'],
                "phone" => $_GET['phone'],
                "address" => $_GET['address'],
            ); 
            $email->sendCheckoutEmailStore($message);
            $email->sendCheckoutEmailCustomer($message);
            return wp_send_json($email->response);
        },
    )); 

    /*
    Promos
    */
    register_rest_route( 'info/v2', '/get/promotion', array(
        'methods' => 'GET',
        'callback' => function(){

            $api = new info();
            $api->getPromotion();

            return wp_send_json($api->response);
        },
    ));

    register_rest_route( 'info/v2', '/get/info/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => function($data){

            $api = new info();
            $api->getInfo($data["id"]);

            return wp_send_json($api->response);
        },
    )); 

    /*
    Other
    */
    register_rest_route( 'other/v2', '/celebrities', array(
        'methods' => 'GET',
        'callback' => function(){

            $api = new other();
            $api->getCelebrities();

            return wp_send_json($api->response);
        },
    ));
});
