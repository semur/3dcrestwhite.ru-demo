<?php
/*
##################################################################
Classes for customers
##################################################################
*/
class customers {
   	
   	/*
	Constructor for defining globals
	*/
	var $response = array(); 

	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	}


 	function customerAdd($options){

		$post_id = wp_insert_post(array (
			'post_type' 	=> 'customers',
			'post_title' 	=> $options->name, 
			'post_status' 	=> 'publish',
			'comment_status'=> 'closed',    
			'ping_status' 	=> 'closed',       
		));  
	  
		$str = "";
  		$has_ordered = $this->customerAddHelper($post_id, $options->email);
  		if($has_ordered){
  			$str .= "\n";
  			$str .= "Повторный заказ от этого клиента \n";
  		}
    
	 	foreach ($options->products as $key => $val) { 
	 		$val = json_decode(stripslashes($val));   
    
	 	 	$str .= "Product: ".$val->name."\n";
	 	 	$str .= "Quantity: ".$val->quantity."\n\n";  
	 	} 
	 	$options->message = $str;
 		   
		update_field('customers_phone', $options->phone, $post_id);
		update_field('customers_email', $options->email, $post_id);
		update_field('customers_address', $options->address, $post_id);
		update_field('customers_name', $options->name, $post_id);
		update_field('customers_message', $options->message, $post_id);

 		$this->response->success = TRUE;
 		$this->response->data = $post_id;
 	}  

 	function customerAddHelper($id, $email){

 		$posts = get_posts(array(
			'numberposts'	=> -1,
			'post_type'		=> 'customers',
			'meta_key'		=> 'customers_email',
			'meta_value'	=> $email
		)); 
		if(count($posts) > 0){
			update_field('customers_has_ordered_before', 1, $id);  
			return TRUE;
		}
		return FALSE;
 	}	 

 	function callbackAdd($message){
   
		$message->phone = esc_attr($message->phone); 
		$post_id = wp_insert_post(array (
			'post_type' 	=> 'callbacks',
			'post_title' 	=> $message->phone, 
			'post_status' 	=> 'publish',
			'comment_status'=> 'closed',    
			'ping_status' 	=> 'closed',       
		));   

 		$this->response->success = $success;
 		$this->response->data = $post_id;
 	}   
}  