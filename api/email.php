<?php
/*
##################################################################
Classes for emails
##################################################################
*/
class emails {
   	
   	/*
	Constructor for defining globals
	*/
	var $response = array(); 

	/*
	Constructor for defining globals
	*/
	function __construct(){
		$this->response = (object)array(
			'success' => FALSE,
			'data' 	  => array(),
		);  
	} 

 	function callbackEmail($message){

		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$body = '
 		<div class="main-wrap" style="width: 100%;display: table;padding: 40px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	         
	        <div class="quote-icons" style="display: table;width: 100%;color: #fff;clear: both;min-height: 300px;position: relative;padding-bottom: 60px;max-width: 800px;background: #fff;margin: 0 auto;border-radius: 4px;padding: 4px;box-shadow: 0px 0px 5px 0 #ccc;">

	            <div class="header" style="width: 100%;display: table;background: #fff;height: 40px;padding: 5px;border-bottom: 1px solid #f2f2f2;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	                <a href="'.site_url().'" style="height: 100%;display: table;">
	                    <img src="'.get_template_directory().'/public/img/other/logo.png" style="width: 220px;">
	                </a> 
	            </div>

	            <h2 id="title-text" style="text-align: center;font-family: Arial;font-weight: 200;text-transform: uppercase;font-size: 25px;margin: 10px 0;margin-bottom: 20px;margin-top: 40px;color: #000;padding: 0 40px;">
	                Заказ звонка
	            </h2>
 
				<div class="content" id="content-text" style="color: #000;font-size: 13px;font-family: Arial;display: table;width: auto;margin: 0 auto;text-align: left;line-height: 18px;padding: 20px;">
				                  
					<div>Телефон: '.$message->phone.' </div>
  
	            </div> 
	        </div>  
	        
	        <div class="unsubscribe" style="color: #999;font-family: Arial;padding: 40px;font-size: 12px;margin: 0 auto;width: 100%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-bottom: 0;">
	            Это сообщение отправлено с сайта '.site_url().'
	        </div>
	    </div>
 		'; 
   		$response = wp_mail( 
   			get_field("global_cart_email","options"), //$to
   			"Заказ звонка", //$subject 
   			$body, //$message
   			$headers //$headers 
   		); 
   		$this->response->success = TRUE;
 	} 

 	function buildCheckoutMessage($products){
 
   		$str = ""; 
        foreach ($products as $key => $value) { 
	 		$value = json_decode(stripslashes($value));   

        	$str .= "<div>Тип товара: ".$value->name."</div>";
        	$str .= "<div>Количество: ".$value->quantity."</div>";
        	$str .= "<div><a href='".urldecode($value->url)."'>Ссылка</a></div><br>"; 
        } 
        return $str;
 	}

 	function sendCheckoutEmailCustomer($message){
 		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
 		
 		$body = '
 		<div class="main-wrap" style="width: 100%;display: table;padding: 40px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
         
	        <div class="quote-icons" style="display: table;width: 100%;color: #fff;clear: both;min-height: 300px;position: relative;padding-bottom: 60px;max-width: 800px;background: #fff;margin: 0 auto;border-radius: 4px;padding: 4px;box-shadow: 0px 0px 5px 0 #ccc;">

	            <div class="header" style="width: 100%;display: table;background: #fff;height: 40px;padding: 5px;border-bottom: 1px solid #f2f2f2;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	                <a href="'.site_url().'" style="height: 100%;display: table;">
	                    <img src="'.get_template_directory().'/public/img/other/logo.png" style="width: 220px;">
	                </a> 
	            </div>

	            <h2 id="title-text" style="text-align: center;font-family: Arial;font-weight: 200;text-transform: uppercase;font-size: 25px;margin: 10px 0;margin-bottom: 20px;margin-top: 40px;color: #000;padding: 0 40px;">
	                Подтверждение заказа
	            </h2>

	            <div class="content" id="content-text" style="color: #000;font-size: 13px;font-family: Arial;display: table;width: auto;margin: 0 auto;text-align: left;line-height: 18px;padding: 20px;">
	                Спасибо за ваше доверие! Наш менеджер свяжется с вами чтобы решить наиболее удобное время доставки.
	            </div> 
	            <br>
	            <div class="small" id="small" style="color: #000;font-size: 13px;font-family: Arial;padding: 0 100px;text-align: center;">
	                Если вам интересно... мы вернем 300 RUB с вашей покупки - о подробностях пишите на - <a href="'.get_field("global_instagram_link","options").'">Instagram</a>, на Whatsapp <a href="tel:'.get_field("global_phone","options").'">'.get_field("global_phone","options").'</a>, либо менеджеру который с вами свяжется по подбору вашего заказа.
	            </div>
	    		
	            
	            <a class="compare" id="button-text" href="'.get_permalink(682).'" style="margin: 20px auto;margin-top: 10px;font-family: Arial;font-weight: 300;font-size: 20px;color: #fff;display: table;border-radius: 2px;text-decoration: inherit;position: relative;text-shadow: inherit;border: 0 solid transparent;height: 50px;width: 300px; text-align: center;padding: 10px 5px; line-height:45px;box-shadow: none !important;background: #B80707;color:#fff;border-radius: 40px;">
	                инструкция по отбеливанию
	            </a>

	            <div class="cap wrap" id="tiles-text" style="width: auto;margin: 0 auto;display: table;padding: 0 40px;margin-top: 40px;max-width: 1000px;text-align: center;padding-bottom: 10px;">
	                <div style="float: left;width: 30%;background: #333;height: 300px;margin-right: 5%;">
	                    <div class="icon-land" style="display:block;width: 100%;height: 160px;margin: 0 auto;overflow: hidden;position: relative;padding-bottom: 10px;">
	                        <div class="mini-overlay" style="background: linear-gradient(to bottom, rgba(0,0,0,0) 50%,rgba(0,0,0,0) 56%,rgba(0,0,0,0.65) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#00000000\', endColorstr=\'#a6000000\',GradientType=0 );position: relative;z-index: 10;width: 100%;height: 130%;overflow: hidden;"></div>
	                        <p style="margin:0;"><img height="200" src="'.get_template_directory().'/public/img/models/icon-left.png" style="width: auto;height: 200px;top: 0;left: -100%;right: -100%;bottom: 0;margin: 0 auto;position: absolute;display: block;z-index: 3;"></p>
	                        
	                    </div>
	                    <h3 style="text-align: center;font-family: Arial;font-size: 15px;text-transform: uppercase;font-weight: bold;padding: 20px;padding-bottom: 10px;padding-top: 0;position: relative;margin: 0;color: #fff;">возврат денег</h3>
	                    <p style="text-align: center;font-family: Arial;display: table;width: auto;margin: 0 auto;color: #fff;font-size: 12px;line-height: 14px;padding: 20px;font-weight: 500;padding-top: 10px;">
	                        Мы вернем 300 RUB с твоей покупки
	                </p></div>
	                <div style="float: left;width: 30%;background: #333;height: 300px;">
	                    <div class="icon-model" style="display:block;width: 100%;height: 160px;margin: 0 auto;overflow: hidden;position: relative;padding-bottom: 10px;">
	                        <div class="mini-overlay" style="background: linear-gradient(to bottom, rgba(0,0,0,0) 50%,rgba(0,0,0,0) 56%,rgba(0,0,0,0.65) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#00000000\', endColorstr=\'#a6000000\',GradientType=0 );position: relative;z-index: 10;width: 100%;height: 130%;overflow: hidden;"></div>
	                        <p style="margin:0;"><img height="200" src="'.get_template_directory().'/public/img/models/icon-middle.png" style="width: auto;height: 200px;top: 0;left: -100%;right: -100%;bottom: 0;margin: 0 auto;position: absolute;display: block;z-index: 3;"></p>
	                        
	                    </div>
	                    <h3 style="text-align: center;font-family: Arial;font-size: 15px;text-transform: uppercase;font-weight: bold;padding: 20px;padding-bottom: 10px;padding-top: 0;position: relative;margin: 0;color: #fff;">СКИДКА 10%</h3>
	                    <p style="text-align: center;font-family: Arial;display: table;width: auto;margin: 0 auto;color: #fff;font-size: 12px;line-height: 14px;padding: 20px;font-weight: 500;padding-top: 10px;">Скидка 10% на вторую покупку всем нашим клиентам</p>
	                </div>
	                <div style="float: left;width: 30%;background: #333;height: 300px;margin-left: 5%;"> 
	                    <div class="icon-facade" style="display:block;width: 100%;height: 160px;margin: 0 auto;overflow: hidden;position: relative;padding-bottom: 10px;">
	                        <div class="mini-overlay" style="background: linear-gradient(to bottom, rgba(0,0,0,0) 50%,rgba(0,0,0,0) 56%,rgba(0,0,0,0.65) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#00000000\', endColorstr=\'#a6000000\',GradientType=0 );position: relative;z-index: 10;width: 100%;height: 130%;overflow: hidden;"></div> 
	                        <p style="margin:0;"><img height="200" src="'.get_template_directory().'/public/img/models/icon-right.png" style="width: auto;height: 200px;top: 0;left: -100%;right: -100%;bottom: 0;margin: 0 auto;position: absolute;display: block;z-index: 3;"></p>
	                        
	                    </div>
	                    <h3 style="text-align: center;font-family: Arial;font-size: 15px;text-transform: uppercase;font-weight: bold;padding: 20px;padding-bottom: 10px;padding-top: 0;position: relative;margin: 0;color: #fff;">ПОДАРКИ</h3>
	                    <p style="text-align: center;font-family: Arial;display: table;width: auto;margin: 0 auto;color: #fff;font-size: 12px;line-height: 14px;padding: 20px;font-weight: 500;padding-top: 10px;">При покупке 3 или более пачек 3D-Crest-White прилагается подарок</p>
	                </div> 
	            </div>

	        </div>  
	        
	        <div class="unsubscribe" style="color: #999;font-family: Arial;padding: 40px;font-size: 12px;margin: 0 auto;width: 100%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-bottom: 0;">
	            Это сообщение отправлено с сайта '.site_url().'
	        </div>
	    </div>
 		';
		$response = wp_mail( 
			$message->email, //$to
			"Подтверждение заказа", //$subject 
			$body, //$message
			$headers //$headers 
		); 
   		$this->response->success = $response; 
 	}

 	function sendCheckoutEmailStore($message){

		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
 		
 		$body = '
 		<div class="main-wrap" style="width: 100%;display: table;padding: 40px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	         
	        <div class="quote-icons" style="display: table;width: 100%;color: #fff;clear: both;min-height: 300px;position: relative;padding-bottom: 60px;max-width: 800px;background: #fff;margin: 0 auto;border-radius: 4px;padding: 4px;box-shadow: 0px 0px 5px 0 #ccc;">

	            <div class="header" style="width: 100%;display: table;background: #fff;height: 40px;padding: 5px;border-bottom: 1px solid #f2f2f2;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	                <a href="'.site_url().'" style="height: 100%;display: table;">
	                    <img src="'.get_template_directory().'/public/img/other/logo.png" style="width: 220px;">
	                </a> 
	            </div>

	            <h2 id="title-text" style="text-align: center;font-family: Arial;font-weight: 200;text-transform: uppercase;font-size: 25px;margin: 10px 0;margin-bottom: 20px;margin-top: 40px;color: #000;padding: 0 40px;">
	                Новый заказ через форму с сайта
	            </h2>
 
				<div class="content" id="content-text" style="color: #000;font-size: 13px;font-family: Arial;display: table;width: auto;margin: 0 auto;text-align: left;line-height: 18px;padding: 20px;">
				                 
					<div>От: '.$message->name.'</div>
					<div>Email: '.$message->email.'</div>
					<div>Тел: '.$message->phone.'</div>
					<div>Адрес: '.$message->address.'</div>
					<br>
					'.$message->message.'
 
	            </div> 
	        </div>  
	        
	        <div class="unsubscribe" style="color: #999;font-family: Arial;padding: 40px;font-size: 12px;margin: 0 auto;width: 100%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-bottom: 0;">
	            Это сообщение отправлено с сайта '.site_url().'
	        </div>
	    </div>
 		';
		$response = wp_mail( 
			get_field("global_cart_email","options"), //$to
			"Новый заказ через форму с сайта", //$subject 
			$body, //$message
			$headers //$headers 
		); 
   		$this->response->success = $response;
 	}  
}