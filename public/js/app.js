
/*
Check if it is a touch-supporting phone/tablet/laptop
*/
window.adjustPickFrom = function() {
	
	settings.dbug && console.log("adjustPickFrom");	 


    $image = $("#pick-form #form-image");
    $box   = $("#pick-form #form-box");

    if(!$box.length)
        return;
    
    var image = $image.outerHeight();
    var box = $box.outerHeight()+$box.position().top; 
    
    settings.dbug && console.log(image +" > "+ box);

    $image.imagesLoaded(function(){ 
        if($image.width() < $(window).outerWidth()){

        	$image.css({
        		width:"100%",
        		height:"auto"
        	});

        }else if(image > box){	
        	$image.css({
        		width:"100%",
        		height:"auto"
        	});
        }else{ 
        	$image.css({
        		width:"auto",
        		height:"100%"
        	});
        }
    });
}
setInterval(function(){

    window.adjustPickFrom();
},2000);
  
window.pickFormScroll = function(){

    var $box = $("#pick-form #form-box .tab-group.active .scroll-wrap");
    var $tile = $box.find(".tab-content").last();
    $tile.addClass("red");

    if(!$box.length)
        return;
    
    $box.finish();
    $box.animate({
        scrollTop:99999
    },1000,function(){
          
        setTimeout(function(){
            $tile.removeClass("red");
        },500);
    }); 
}

window.getQuery = function(name) {

    var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location);
    return (results != null) ? (results[1] || 0) : false;
};




app = angular.module("3dApp", ["ngCookies","wu.masonry"]);

app.config(["$sceProvider",function($sceProvider){
    $sceProvider.enabled(false);
}]); 
 


// function debounce(fn, delay) {
//   var timer = null;
//   return function () {
//     var context = this, args = arguments;
//     clearTimeout(timer);
//     timer = setTimeout(function () {
//       fn.apply(context, args);
//     }, delay);
//   };
// }
// $('input.username').keypress(debounce(function (event) {
//   // do the Ajax request
// }, 250)); 