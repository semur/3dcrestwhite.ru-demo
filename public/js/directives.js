app.directive("3dButton", function() {
    return {
        templateUrl : settings.url+"/templates/directives/button.html",     					
        controller : ["$scope", "$rootScope", "cartHelper", "mpHelper", function($scope, $rootScope, cartHelper, mpHelper){
  
             $scope.cartIsLoading = false;
            
            //добавить в корзину

            $scope.addToCartLoop = function($event, product, index){  
                $event.stopPropagation();
                $event.preventDefault();   
                $scope.cartIsLoading = index; 
                cartHelper.addToCartSingle(product).then(function(response){
 
                    settings.dbug && console.log(response);

                    cartHelper.animation($event.currentTarget);
                    $scope.cartIsLoading = false;
                     
                    mpHelper.trackAddToCart({ 
                        from:"Loop",
                        url:location.href, 
                        product: [product.title]
                    });  

                    cartHelper.setCart().then(function(response){
                        $rootScope.quantity = response.quantity;
                        $rootScope.cart = response; 
                        $rootScope.cartIsVisible = response.quantity > 0;  
                    });
                }); 
            } 
            $scope.addToCart = function($event, product){  
              
                $scope.cartIsLoading = true; 
                cartHelper.addToCartSingle(product).then(function(response){
 
                    settings.dbug && console.log(response);

                    cartHelper.animation($event.currentTarget);
                    $scope.cartIsLoading = false;

                    mpHelper.trackAddToCart({ 
                        from:"Single",
                        url:location.href, 
                        product: [product.title]
                    });  

                    cartHelper.setCart().then(function(response){
                        $rootScope.quantity = response.quantity;
                        $rootScope.cart = response; 
                        $rootScope.cartIsVisible = response.quantity > 0;  
                    });
                });
            }

            $scope.cartIsLoadingLoop = function(index){ 
                if($scope.cartIsLoading==index)
                    return "active";
            }

        }],
    };
});