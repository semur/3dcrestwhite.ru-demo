/*
Main controller
*/ 
app.controller("mainController", ["$scope","$rootScope", "$sce", "$http", "cartHelper", "userHelper", "mpHelper", "reviewHelper", "otherHelper", 
	function($scope, $rootScope, $sce, $http, cartHelper, userHelper, mpHelper, reviewHelper, otherHelper) {
 
    settings.dbug && console.log("Main controller");
  	 
    /* Set main user cookie object */ 
	var user = userHelper.get(); 
	userHelper.pageUp();
	userHelper.setCampaign({
        source:window.getQuery("utm_source"),
        medium:window.getQuery("utm_medium"),
        campaign:window.getQuery("utm_campaign"),
        term:window.getQuery("utm_term"), //target of the campaign
    });
 
	$rootScope.user = user;
 
    /* make overlay visible or not */
    $rootScope.overlayVisible = false;
    $rootScope.overlayShow = function(){ 
		$rootScope.overlayVisible = true; 
	}
	$rootScope.overlayHide = function(){
		$rootScope.overlayVisible = false;
	}
	$rootScope.overlayIsVisible = function(){  
		return $rootScope.overlayVisible;
	} 

	/*
	Navigation controls
	*/ 
   	$rootScope.menuID = false;
   	$rootScope.navigationTrigger = function(id){ 
   		$rootScope.menuID = id;  
   	}
   	$rootScope.navigationDropdown = function(id){  
   		return $rootScope.menuID == id;
   	}
   	$rootScope.navigationDropdownIsActive = function(id){  
   		if($rootScope.menuID == id)
   			return "active";
   	}

   	/*
	Set cart object
   	*/
   	$rootScope.cartIsVisible = false; 
   	$rootScope.showIsActive = function(){
   		if(!$rootScope.cartIsVisible)
   			return "disabled";
   	}; 
   	cartHelper.setCart().then(function(response){
   		$rootScope.quantity = response.quantity;
   		$rootScope.cart = response;	
	   	$rootScope.cartIsVisible = response.quantity > 0;   
   	});

   	$rootScope.celebritiesClick = function(){
        mpHelper.trackCelebritiesClick({ 
            from:location.href, 
        });  
   	}



   	/* 
 	Animate carousel
	Find timeout value, timeout by that, and then set interval to repeat.
 	Go thorugh all tiles, delete and reinsert the classes on interval to reset animation
 	*/

   	$scope.celebrities = [];
   	otherHelper.getCelebrities().then(function(response){

   		if(response)
	   		$scope.celebrities = response;
   	}); 

 	var $delay = $("[data-bound]");
 	var delay = parseInt($delay.attr("data-bound"))*1000; 
 	var classes = [];
    setTimeout(function() {

        setInterval(function() { 
        	$delay.animate({
        		opacity:0
        	},1000,function(){
        		$delay.hide();

	        	$elem = $delay.find("a");
	        	$elem.each(function(){

			        var temp = $(this).attr("class");
	    			$(this).attr("class","");
	    			classes.push(temp); 
	        	});

			    setTimeout(function() {

		        	$elem = $delay.find("a");
		        	var index = 0;
		        	$elem.each(function(){ 
		        		$(this).attr("class",classes[index]);
		        		index++;
		        	}); 
        			
        			$delay
	        			.show()
			        	.animate({
		        			opacity:1
		        		},1000);

			    }, 500);
			});
	    }, delay+5000);
    }, delay);



    /* 
	Instagram popup
    */ 

	$scope.fold = true;
	$scope.isFolded = function(){

		if($scope.fold)
			return "folded";
	}
	$scope.changeFold = function(){

		$scope.fold = !$scope.fold;
	}
  
	$scope.displayReviewsAll = function(){
		return true;
	}; 
	$scope.getReviewsAll = settings.staticData.instagramReviews;  

	$scope.displayReview = false; 
	$scope.displayReviewSingle = function(){ 
        return $scope.displayReview
	}; 
	$scope.displayReviewLoad = true; 
	$scope.displayReviewLoading = function(){
		return $scope.displayReviewLoad;
	};  
	$scope.showReview = function($event, index){ 
		
		//only on desktops!
		if($(window).outerWidth() >= 635){ 

			$event.stopPropagation();
	        $event.preventDefault();  
 
	   		$scope.displayReviewLoad = true;
	   		$scope.displayReview = true;
			$rootScope.overlayShow();

		   	reviewHelper.getCurrentReview($scope.getReviewsAll[index].id).then(function(response){
		   		$scope.review = response;  
		   		$scope.displayReview = true;
		   		$scope.displayReviewLoad = false;
		   	}); 
	    }
	}
 
	$scope.hideReview = function(){
  
		$scope.displayReview = false;
		$rootScope.overlayHide();
	}   

	/* Callback form */
	$scope.callbackSubmitted = false;
   	$scope.addToCallbacks = function(customer){
 
		/*
		Send email about callback
		*/  
   		$http({
			method: 'GET', 
			url: '/wp-json/customers/v2/callback/add',
			params: { 
				phone: customer.phone
			} 
		}).then(function successCallback(response) {

   			$scope.callbackSubmitted = true;
			settings.dbug && console.log(response); 

		}, function errorCallback(response) {

			settings.dbug && console.log("error");
		}); 
   	}

   	$scope.addSpinner = function($event){

   		$($event.target).addClass("rotate");
   	}


   	/*
	Sidebar and other small loads
   	*/
 
   	$scope.showPickFormImage = function(){
 
   		if($(window).width() >= 780)
		    return $sce.trustAsHtml(settings.pickFormImage);  
		return false;
   	}


	// /*
	// Horizontal wrap vs masonry
	// */
	// $scope.isMobile = function(){
	// 	return $(window).width() < 780;
	// }
	// $scope.isDesktop = function(){ 
	// 	return !$scope.isMobile();
	// }


	/*
	Used to be loaded synamically. Now static data
	*/
	$rootScope.outerTabs = {
		fast : settings.staticData.pickForm.fast,
		comfortable : settings.staticData.pickForm.comfortable,
		results : settings.staticData.pickForm.results,
		fortify : settings.staticData.pickForm.fortify[0],
		relief : settings.staticData.pickForm.relief[0],
	};  
	

}]);
 

/*
Controller for handling live reviews (archive)  
*/ 
app.controller("liveReviewsControllerArchive", ["$scope", "$rootScope", "$http", "reviewHelper", function($scope, $rootScope, $http, reviewHelper) {
   	
	// 	$scope.display= false; 
	// 	$scope.displayReview = function(){
	// 		return $scope.display;
	// 	}; 
		
	// 	$scope.getReviewsAll = []; 
	// $http({
	// 	method: 'GET',
	// 	url: '/wp-json/instagram/v2/live/get/all', 
	// }).then(function successCallback(response) {

	// 	$scope.getReviewsAll = response.data.data; 
	//    	$scope.display = true;
	 
	// }, function errorCallback(response) {

	// 	settings.dbug && console.log("error"); 
	// }); 

	// $scope.fold = true;
	// $scope.isFolded = function(){

	// 	if($scope.fold)
	// 		return "folded";
	// }
	// $scope.changeFold = function(){

	// 	$scope.fold = !$scope.fold;
	// }

	// $scope.showReview = function($event, index){ 
		
	// 	//only on desktops!
	// 	if($(window).outerWidth() >= 635){ 

	// 		$event.stopPropagation();
	//         $event.preventDefault(); 
	// 		reviewHelper.setCurrentReview($scope.getReviewsAll[index].id);
	// 		reviewHelper.show();  
	// 		$rootScope.overlayShow();
	//     }
	// }
   	
}]);


/*
Controller for handling live reviews (single)  
*/  
app.controller("liveReviewsControllerSingle", ["$scope", "$http", "$rootScope", "reviewHelper", function($scope, $http, $rootScope, reviewHelper) {

	// $scope.display = false;
	// 	$scope.displayReview = function(){ 
	// 		return $scope.display; 
	// }

	// $scope.review = null;
	// 	reviewHelper.getCurrentReview().then(function(response){
	// 		$scope.review = response; 
	// 		$scope.display = true;
	// 	});

	// 	$scope.isVisible = function(){
	// 		return reviewHelper.isVisible(); 
	// }
	// $scope.hideReview = function(){

	// 	reviewHelper.hide();  
	// 	$rootScope.overlayHide();
	// }
}]);


/*
Controller for handling navigation
*/ 
app.controller("navigationController", ["$scope", "$http", "$rootScope", "cartHelper", function($scope, $http, $rootScope, cartHelper) {
   	  
   	
}]);

 
/*
Controller for handling faq pages
*/ 
app.controller("faqController", ["$scope", "$http", "$rootScope", "$sce", 
	function($scope, $http, $rootScope, $sce) {
 			   
 	$scope.faqs = [];
 	for(var faq in settings.faqs){
 		faq = settings.faqs[faq]; 
 		$scope.faqs.push({
 			title: faq.title,
 			tags: faq.tags,
 			content: $sce.trustAsHtml(faq.content),
 		});
 	}  	   
	$scope.getFaqs = function(){  

		return $scope.faqs;
	} 

	$scope.getFaqsCategories = function(){

		return settings.faqsCategories; 
	}
 	
    $scope.selectedGenres = settings.faqsCategories;

    $scope.faqInactive = true;
    $scope.faqIsInactive = function(){
    	if($scope.faqInactive)
    		return "active";
    }
    $scope.removeFaqsFilter = function(){

    	$scope.faqInactive = !$scope.faqInactive;
    }
    
    $scope.addFaqsFilter = function(category){  

    	for(var tag in $scope.selectedGenres){
    		tag = $scope.selectedGenres[tag];
    		tag.selected = 0;
    	}
    	$scope.faqInactive = false;
    	$scope.selectedGenres[category].selected = !$scope.selectedGenres[category].selected;  
    }
 
    $scope.faqIsActive = function(category){

    	if($scope.selectedGenres[category].selected)
    		return "active";
    }

    $scope.filterByGenres = function(items) {

    	if($scope.faqInactive){ 
    		return true;
    	}

    	//foreach all tags from FAQ item tags
    	for(var tag in items.tags){
    		var index = tag;
    		tag = items.tags[tag];

    		//get selected genres, which is all of the categories
    		//get the name, check if that name is equal to any of the  
    		if($scope.selectedGenres[tag].selected){ 
    			return true;
    		}
    	} 
    	return false;
    };
	 
}]);

/*
Controller for handling info messages
*/ 
app.controller("infoController", ["$scope", "$attrs", "$http", "$timeout", "infoHelper", "mpHelper", "userHelper", function($scope, $attrs, $http, $timeout, infoHelper, mpHelper, userHelper) {
 
	// userHelper.remove();
	// userHelper.reset();
  	$scope.activeInfo = false;
	$scope.info = "";
	$scope.type = "";
	var activeTimeout = false;

		settings.dbug && console.log(userHelper.get());

	$scope.enable = true;

	// if($attrs.pageid !== undefined && $attrs.pageid!=null && $attrs.pageid!="")
	// 	infoHelper.checkInfo($attrs.pageid).then(function(message){

	//     	activeTimeout = message.enable;
	//         $scope.info = message.message;
	//         $scope.type = message.type; 
	//         $scope.timeout = message.timeout; 
	// 	});

   	$scope.infoIsActive = function(){ 
  		
  		return false;
     //    var dynamicEnabled = infoHelper.dynamicEnabled($attrs.pageid);
     //    var defaultEnabled = infoHelper.defaultEnabled();

     //    if(!dynamicEnabled && !defaultEnabled)
     //        $scope.enable = false;

     //    // console.log($scope.activeInfo +" "+ !$scope.activePromotion +" "+ $scope.enable);
   		// return $scope.activeInfo && !$scope.activePromotion && $scope.enable && false;//this nees to be rewritted, no good
   	}
   	$scope.infoClose = function(){ 
   
		$scope.activeInfo = false;

		if($scope.type=="default")
			userHelper.updateInfoDefault(false);  
		else
			userHelper.updateInfoDynamic(false, $attrs.pageid);  
 
		// mpHelper.trackInfoDropdown({
		// 	target:"Info",
		// 	message: settings.infoMessage //this message is inserted from the current template
		// });
   	} 


   	/*
	Check if can display promotion if it active from Admin and if user hasnt closed it
  	*/
	$scope.activePromotion = false;
	$scope.promotion = "";
	$http({
		method: 'GET', 
		url: '/wp-json/info/v2/get/promotion', 
	}).then(function successCallback(response) {
   			   
			response = response.data.data;
			$scope.activePromotion = response.status && !userHelper.get().promoDropdownHidden; 
			$scope.promotion = response.promotion;   

	}, function errorCallback(response) {

		settings.dbug && console.log("error");
	}); 

   	$scope.promotionIsActive = function(){ 
   		return $scope.activePromotion;
   	}
   	$scope.promotionClose = function(){ 
  
		userHelper.update("promoDropdownHidden",true);  
		$scope.activePromotion = false;

		mpHelper.trackInfoDropdown({
			target:"Promotion"
		});


        $timeout(function(){ 
	        $scope.activeInfo = activeTimeout; 
	    },$scope.timeout); 
   	}
}]);




/*
Controller for handling callbacks
*/ 
app.controller("callbackController", ["$scope", "$http", "$rootScope", "cartHelper", function($scope, $http, $rootScope, cartHelper) {
  	 
	
}]);

/*
Controller for handling the popup form for instagram feed
*/ 
app.controller("popupInstagramController", ["$scope", "$http", "$rootScope", "$sce", "$interval", "$timeout", "userHelper", function($scope, $http, $rootScope, $sce, $interval, $timeout, userHelper) {
   
	$scope.popupInstagramVisible = false;
	$scope.popupInstagramVisibleLoader = false;
    $scope.popupInstagramShow = function(){
 
    	//set cookie
	    $scope.popupInstagramVisible = true;

	    if(!$scope.popupInstagramRemoved){
 			
 			$scope.popupInstagramVisibleLoader = true;
			$http({
			  method: 'GET',
			  url: '/wp-json/instagram/v2/static/get/all'
			}).then(function successCallback(response) {

				$scope.popupInstagramVisibleLoader = false;
		    	settings.dug && console.log(response);
		    	var data = response.data.data;
		    	for(var val in data){
		    		val = data[val]; 
		    		val.excerpt = $sce.trustAsHtml(val.excerpt);
		    	}
				$scope.instagramFeedStatic = data;

			}, function errorCallback(response) {

				settings.dbug && console.log("error");
			});
	    }
    } 

    $scope.popupInstagramFold = function(){
 
	    $scope.popupInstagramVisible = false;
    } 

    //display instagram feed onless user has clicked to hide it
    $scope.popupInstagramRemoved = userHelper.get().instagramFeedHidden;
    $scope.popupInstagramClose = function(){

    	//set cookie 
		userHelper.update("instagramFeedHidden",true);  
	    $scope.popupInstagramRemoved = true;
    } 

    // shaker
    $scope.shaker = false;
    $interval(function(){
    	$scope.shaker = true;  
    },settings.shakerInterval);
    $timeout(function(){ 
    	$interval(function(){
	    	$scope.shaker = false;  
	    },settings.shakerInterval);
	},settings.shakerInterval-2000);

    $scope.shake = function(){
    	return $scope.shaker;
    } 
}]);

/*
Controller for single pages
*/ 
app.controller("singleController", ["$scope", "$http", "cartHelper", "otherHelper", function($scope, $http, cartHelper, otherHelper) {
 	
   	/*
	Sidebar and other small loads
   	*/
   	$scope.currentTab = 1;
   	$scope.tabChange = function(index){
	   	$scope.currentTab = index;
   	}
   	$scope.tabIsActive = function(index){
	   	if($scope.currentTab == index)
	   		return "active";
   	}


   	$scope.currentFaq = 0;
   	$scope.faqShow = function(index){
	   	$scope.currentFaq = index;
   	}
   	$scope.faqIsActive = function(index){
	   	if($scope.currentFaq == index)
	   		return "active";
   	}

   	

}]);  


/*
Controller for loop content
*/ 
app.controller("loopController", ["$scope", "$http", "cartHelper", function($scope, $http, cartHelper) {

	// $scope.cartIsLoading = false; 
	// $scope.addToCartLoop = function($event, id){ 
		
	// 	cartHelper.animation($event.currentTarget);
	// 	$event.stopPropagation();
 //        $event.preventDefault();
	// }

}]);   
   
/*
Controller for handling the form on home page that helps pick the right treatment
*/ 
app.controller("pickFormController", ["$scope", "$rootScope", "$http", "cartHelper", "mpHelper", function($scope, $rootScope, $http, cartHelper, mpHelper) {

	window.adjustPickFrom(); 
	$scope.$on('$viewContentLoaded', function() {

		window.adjustPickFrom(); 
	});

	$scope.activeOuterTab = 0;
	$scope.outerTabActive = function(index){

		return $scope.activeOuterTab == index;
	}
	$scope.outerTabChange = function(index){

		$scope.activeOuterTab = index; 
		$scope.currentProduct = cartHelper.currentProduct($rootScope.outerTabs, $scope.activeOuterTab, $scope.upsellAdded, 0);
		$scope.getTotalPrice = cartHelper.getTotalPrice($scope.currentProduct);
		window.adjustPickFrom();

		mpHelper.trackPickForm({ 
			clickOuter:$scope.activeOuterTab,   
		});	 
	} 

	$scope.activeInnerTab = 0;
	$scope.innerTabActive = function(index){  

		return $scope.activeInnerTab == index;
	}
	$scope.innerTabChange = function(index){
		$scope.activeInnerTab = index;   
		
		$scope.currentProduct = cartHelper.currentProduct($rootScope.outerTabs, $scope.activeOuterTab, $scope.upsellAdded, index);
		$scope.getTotalPrice = cartHelper.getTotalPrice($scope.currentProduct);
		window.adjustPickFrom();
		window.pickFormScroll();

		settings.dbug && console.log("Current Product [inner tab]");
		settings.dbug && console.log($scope.currentProduct);

		mpHelper.trackPickForm({
			clickOuter:$scope.activeOuterTab,
			clickInner:$scope.activeInnerTab,  
		});	 
	} 

	$scope.upsellAdded = [false,false];
	$scope.upsellActive = function(index){

		return $scope.upsellAdded[index];
	}
	$scope.changeUpsell = function(i){

		$scope.upsellAdded[i] = !$scope.upsellAdded[i];
		var index = $scope.activeInnerTab;
		$scope.currentProduct = cartHelper.currentProduct($rootScope.outerTabs, $scope.activeOuterTab, $scope.upsellAdded, index);
		$scope.getTotalPrice = cartHelper.getTotalPrice($scope.currentProduct);
		window.adjustPickFrom();
		window.pickFormScroll();

		settings.dbug && console.log("Current Product [upsell]");
		settings.dbug && console.log($scope.currentProduct);

		var upsell;
		if(i==0)
			upsell = "Fortify";
		if(i==1)
			upsell = "Relief";

		mpHelper.trackPickForm({
			clickOuter:$scope.activeOuterTab,
			clickInner:$scope.activeInnerTab,  
			clickUpsell:upsell,   
		});	 
	}


	$scope.currentProduct = cartHelper.currentProduct($rootScope.outerTabs, $scope.activeOuterTab, $scope.upsellAdded, 0); 
	$scope.getTotalPrice = cartHelper.getTotalPrice($scope.currentProduct);
 
	$scope.formIsLoaded = function(){
		return true;
	} 	 
   
	$scope.cartIsLoading = false;
	$scope.addToCartPickForm = function($event){

		$scope.cartIsLoading = true;

		cartHelper.addToCart($scope.currentProduct).then(function(response){

			$scope.cartIsLoading = false;
			cartHelper.animation($event.currentTarget);

			if(response==-1){
				settings.dbug && console.log("ajax error"); 
				
			}else{
				
				/*
				Add tracking events
				*/
				var count = 0;
				var product = $scope.currentProduct;
				var products = [];
				var productsTrack = [];
				if(product.main!=null){
					products.push({
						id:product.main.id,
						price:product.main.price,
						title:product.main.title,	
					}); 
					productsTrack.push(product.main.title);
				}
				if(product.fortify!=null){
					products.push({
						id:product.fortify.id,
						price:product.fortify.price,
						title:product.fortify.title,	
					}); 
					productsTrack.push(product.fortify.title);
				}
				if(product.relief!=null){
					products.push({
						id:product.relief.id,
						price:product.relief.price,
						title:product.relief.title,	
					}); 
					productsTrack.push(product.relief.title);
				}
				settings.dbug && console.log("Adding to cart");
					settings.dbug && console.log(products);

				mpHelper.trackAddToCart({ 
					from:"Pick Form",
					url:location.href,  
					product: productsTrack
				});	 

			   	cartHelper.setCart().then(function(response){
			   		$rootScope.quantity = response.quantity;
			   		$rootScope.cart = response;	
				   	$rootScope.cartIsVisible = response.quantity > 0;  

					settings.dbug && console.log("Setting cart");
 					settings.dbug && console.log(response);
			   	});
			}
		});
	}

	// $http({
	//   method: 'GET',
	//   url: '/wp-json/pick-form/v2/products'
	// }).then(function successCallback(response) {

	// 	var data = response.data.data;
	// 	var productTabs = {
	// 		fast : data.fast,
	// 		comfortable : data.comfortable,
	// 		results : data.results,
	// 		fortify : data.fortify[0],
	// 		relief : data.relief[0],
	// 	}
	// 	$scope.outerTabs = productTabs;

	// 	var index = 0;
	// 	$scope.currentProduct = cartHelper.currentProduct($scope.outerTabs, $scope.activeOuterTab, $scope.upsellAdded, index); 
	// 	$scope.getTotalPrice = cartHelper.getTotalPrice($scope.currentProduct);

	// 	$scope.formLoaded = true;

	// 	$scope.addToCart = function($event){

	// 		$scope.cartIsLoading = true;

	// 		cartHelper.addToCart($scope.currentProduct).then(function(response){

	// 			$scope.cartIsLoading = false;
	// 			cartHelper.animation($event.currentTarget);

	// 			if(response==-1){
	// 				settings.dbug && console.log("ajax error"); 
					
	// 			}else{
					
	// 				/*
	// 				Add tracking events
	// 				*/
	// 				var count = 0;
	// 				var product = $scope.currentProduct;
	// 				var products = [];
	// 				var productsTrack = [];
	// 				if(product.main!=null){
	// 					products.push({
	// 						id:product.main.id,
	// 						price:product.main.price,
	// 						title:product.main.title,	
	// 					}); 
	// 					productsTrack.push(product.main.title);
	// 				}
	// 				if(product.fortify!=null){
	// 					products.push({
	// 						id:product.fortify.id,
	// 						price:product.fortify.price,
	// 						title:product.fortify.title,	
	// 					}); 
	// 					productsTrack.push(product.fortify.title);
	// 				}
	// 				if(product.relief!=null){
	// 					products.push({
	// 						id:product.relief.id,
	// 						price:product.relief.price,
	// 						title:product.relief.title,	
	// 					}); 
	// 					productsTrack.push(product.relief.title);
	// 				}
	// 				settings.dbug && console.log("Adding to cart");
 // 					settings.dbug && console.log(products);

	// 				mpHelper.trackAddToCart({ 
	// 					from:"Pick Form",
	// 					url:location.href,  
	// 					product: productsTrack
	// 				});	 

	// 			   	cartHelper.setCart().then(function(response){
	// 			   		$rootScope.quantity = response.quantity;
	// 			   		$rootScope.cart = response;	
	// 				   	$rootScope.cartIsVisible = response.quantity > 0;  

	// 					settings.dbug && console.log("Setting cart");
	//  					settings.dbug && console.log(response);
	// 			   	});
	// 			}
	// 		});
	// 	}

	// }, function errorCallback(response) {

	// 	settings.dbug && console.log("error");
	// });

}]);   
 


/*
Controller for the comapre box pages
*/ 
app.controller("compareBoxController", ["$scope", "mpHelper", function($scope, mpHelper) { 

	$scope.currentIndex = 0;
	$scope.compareBoxShow = function(index, name){
		$scope.currentIndex = index;

		mpHelper.trackCompare({   
			product: name,
		});	
	}
	$scope.compareBoxIsVisible = function(index){
		return index == $scope.currentIndex;
	}

	/*
	Run some animations to give a hint
	*/
	var timeout = 0;
	$("#title-wrap .product").each(function(){
	  var $elem = $(this);
	  setTimeout(function(){
	    $elem.trigger("click");
	  },timeout);

	  timeout+=200;
	});

}]);

/*
Controller for the cart page
*/ 
app.controller("cartController", ["$scope", "$interval", "$timeout", "emailHelper", "cartHelper", "mpHelper", "otherHelper",
	function($scope, $interval, $timeout, emailHelper, cartHelper, mpHelper, otherHelper) { 
 	
 	cartHelper.getCart().then(function(response){
 		if(response)
		    mpHelper.trackCartView({ 
                quantity: response.quantity,
                total: response.total,
                contents: response.contents
		    });
 	});
 
	/*
	Checkout complete or not
	*/
	$scope.orderComplete = false;
	$scope.checkoutFormShow = function(){
		return $scope.orderComplete;
	}
	$scope.promptShow = function(){
		return !$scope.orderComplete;
	}

	if($(window).width() < 780)
		cartHelper.checkCelebrities().then(function(response){

			if(response)
	            mpHelper.trackCelebritiesShow({
	                //nothing
	            });
		});

	$scope.showCelebrities = function(){
		
		if(cartHelper.celebritiesFolded || $scope.cartEmpty || $scope.orderComplete)
			return "folded"; 
	}

	/*
	Outer tab
	*/
	$scope.city = 0;
	$scope.cityActive = function(index){
		return $scope.city==index;
	}
	$scope.cityChange = function(index){
		return $scope.city=index;
	}

	/*
	Inner tab
	*/
	$scope.paymentMethod = 0;
	$scope.paymentMethodActive = function(index){
		return $scope.paymentMethod==index;
	}
	$scope.paymentMethodChange = function(index){
		return $scope.paymentMethod=index;
	}

	/*
	Hide cart if empty message present
	*/
	$scope.cartEmpty = $("#cart-contents .cart-empty").length;
	$scope.cartIsEmpty = function(){
		//wait for cart to load?
		return $scope.cartEmpty;
	};

	/*
	Run submit form for emails
	*/  
	$scope.email = "";
	$scope.cartIsLoading = false; 
	$scope.checkoutFormEmail = function(customer){
   		 
		$("form .error").removeClass("active");

		var valid = false;
	 	if(customer!==undefined){
		 	if($.trim(customer.email)=="")
		 		$("form .error.email").addClass("active");
		 	if($.trim(customer.phone)=="")
		 		$("form .error.phone").addClass("active");
		 	if($.trim(customer.name)=="")
		 		$("form .error.name").addClass("active");

		 	valid = !($.trim(customer.name)=="" || $.trim(customer.phone)=="" || $.trim(customer.email)=="");
		}else{
			$("form .error").addClass("active");
		}
		if(valid){

			$scope.cartIsLoading = true; 
			emailHelper.checkoutForm(customer).then(function(response){

				if(response){
					$scope.orderComplete = true;
					$scope.cartIsLoading = false;  

					$scope.email = response.email;
				} 
			}); 
		}
	}   
	$scope.signUp = function(){

		mpHelper.trackSignup({
			email:$scope.email
		});
	} 
}]);

 

/*
Controller for the single page image zoom
*/ 
app.controller("zoomController", ["$sce", "$scope", "mpHelper", function($sce, $scope, mpHelper) {
    this.zoomLvl = 5;
 
    var wrap = document.querySelector("#single-head"); 
    wrap = angular.element(wrap)[0]; 
    settings.dbug && console.log(wrap);

    var original = angular.element(wrap.querySelector('#zoom-original'));
    settings.dbug && console.log(original);
    var originalImg = angular.element(original.find('img')); 

    settings.dbug && console.log(originalImg);

    var zoomed = angular.element(wrap.querySelector('#zoom-zoomed'));
    var zoomedImg = angular.element(zoomed.find('img')); 

    var source = originalImg.attr("src-large");

    var mark = angular.element('<div></div>')
        .addClass('mark')
        .css('position', 'absolute')
        .css('height',  '100px')
        .css('width', '100px')

    original.append(mark);
 	
 	var block = false;
 	if($(window).width() < 780)
 		block = true; 
    $(window).resize(function(){
	 	if($(window).width() < 780)
	 		block = true; 
    });

    original
        .on('mouseenter', function(evt) {

        	if(block)
        		return;
            zoomed.removeClass('hide');
            mark.removeClass('hide');
            zoomed.removeClass('hide');

            var offset = calculateOffset(evt);
            moveMark(offset.X, offset.Y);
        })
        .on('mouseleave', function(evt) {

        	if(block)
        		return;
            zoomed.addClass('hide');
            mark.addClass('hide');
            zoomed.addClass('hide');
            
        })
        .on('mousemove', function(evt) {

        	if(block)
        		return;
            var offset = calculateOffset(evt);
            moveMark(offset.X, offset.Y);
        });

    $scope.$on('mark:moved', function(event, data) {
    	if(block)
    		return;
        updateZoomed.apply(this, data);
    });

    function moveMark(offsetX, offsetY) {
        var dx = 100,
            dy = 100,
            x = offsetX - dx / 2,
            y = offsetY - dy / 2;

        mark
            .css('left', x + 'px')
            .css('top', y + 'px');

        $scope.$broadcast('mark:moved', [
            x, y, dx, dy, originalImg[0].height, originalImg[0].width
        ]);
    }

    function updateZoomed(originalX, originalY, originalDx, originalDy, originalHeight, originalWidth) {
        var zoomLvl = 3;
  
        $scope.$apply(function() {

            zoomedImg
                .attr('src', source)
                .css('height', zoomLvl * originalHeight + 'px')
                .css('width', zoomLvl * originalWidth + 'px')
                .css('left', -zoomLvl * originalX + 'px')
                .css('top', -zoomLvl * originalY + 'px');
        });
    }

    var rect;
    function calculateOffset(mouseEvent) {
        rect = rect || mouseEvent.target.getBoundingClientRect();

        var $box = $(mouseEvent.target).offset(); 
        var shift = $(document).scrollTop(); 

        var offsetX = mouseEvent.clientX - rect.left;
        var offsetY = mouseEvent.clientY - rect.top + shift;

        return {
            X: offsetX,
            Y: offsetY
        }
    }

    $scope.overviewHidden = false;
     
    $scope.overviewShow = function(){ 
        $scope.overviewHidden = false;
    } 
    $scope.overviewHide = function(name){ 
        $scope.overviewHidden = true;
        mpHelper.trackZoom({ 
			target:name,  //product name 
		});	
    }
    $scope.getClass = function(){

        if($scope.overviewHidden && !block)
            return "hide";
        return "";
    }
 
}]);