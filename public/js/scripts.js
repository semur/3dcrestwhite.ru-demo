
/*
Global Functions
*/
window.exists = function(elem){ 

	return elem!==undefined && elem!==null && elem!="";
}
 

$(document).ready(function() {  
   
	$.fn.runMasonry = function () {
		
		if($(window).width() >= 650){
			$container = $(this); 
		    $container.imagesLoaded(function() {
 				
		        $container.masonry({
		            isFitWidth: true, 
		            itemSelector: "article"
		        });    
		    }); 

		    $(window).resize(function(){

				if($(window).width() < 650)
					$container.masonry('destroy');
		    }); 
		}  
	}  
 
}); 
 
   
$(document).ready(function() { 

	/*
	################################################
	JS MASONRY INITIATION
	################################################
	*/  
	// var set = 0;
	// $('#js-masonry').runMasonry();  
	// var inter = setInterval(function(){ 
	// 	$('#js-masonry').runMasonry(); 
	// 	set++;

	// 	if(set > 8)
	// 		clearInterval(inter);
	// },2000);


	/*
	################################################
	MOBILE MOMENTUM
	################################################
	*/  
	// $('#instagram-scroller').runMomentum({
	// 	listener:$('#instagram-scroller'),
	// 	y:true,
	// 	x:false,
	// }); 
	/*
	################################################
	MOBLILE MENU
	################################################
	*/
	(function() { 
	    $("#mmenu").mmenu({
		    "extensions": [
		        "pagedim-black"
		    ],
		    navbar:{
		    	add:true,
		    	title:"Меню"
		    },
		    "navbars": [
		        true, {
		            "position": "bottom",
		            "content": [  
		                "<a class='fa fa-instagram' href='"+settings.instagram+"'></a>"
		            ]
		        }
		    ]
		});
		var API = $("#mmenu").data("mmenu");
	    $("#mmenu-trigger").click(function() {

	        API.open();
	    });   	
	})();

  
	/*
	################################################
	PICK FORM IMAGE PARSING
	################################################
	*/
	// (function() { 
 //   		if($(window).width() >= 780) {
	// 	    $("#pick-form-image").html(settings.pickFormImage);
	// 	    alert('done');
 //   		}
	// })();  


	(function() {    
 		

	   	/*
	   	Events
			Add to cart
				from: 
					loop, 
					pick form, 
					single product
				product name: 
					string
			Click on compare products

			Click on sidebar

			Cart page:
				come in, start time
				check out, end time. 

				


	   	*/ 	   	
	})(); 


	/*
	Sidebar menu slider
	*/
	(function() { 
		var $elem = $("#sidebar");
		var visible = false;
		$elem.find("h2").click(function(){

			if(visible){
				$elem.find(".menu").slideUp();
				$(this).find(".arrow").removeClass("down");
			}else{
				$elem.find(".menu").slideDown();
				$(this).find(".arrow").addClass("down");
			}
			visible = !visible;
		});
	})(); 

	/*
	Gloabl bindings
	*/
	(function() {   

	    //reset subnavigation on screen resize 
	    $(window).resize(function() { 
			 // ??? still need this
			$('#horizontal-products').width($(window).outerWidth()); 
		}); 
 		   
	})();   
 
	// -------------------------------------------------------------
	//   Centered Navigation [W]
	// -------------------------------------------------------------
	(function() {
		var $frame = $('#horizontal-products');
		var $wrap = $frame.parent();

		// Call Sly on frame
		$frame.sly({
			horizontal: 1,
			itemNav: 'basic',
			smart: 1,
			// activateOn: 'click',
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 1,
			speed: 300,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1, 
			
			prev: $wrap.find('.prev'),
			next: $wrap.find('.next'),
		});
		$frame.on('load', function(eventName){ 
			$frame.width($(window).outerWidth());
		}); 
		$frame.width($(window).outerWidth());

	}());  


	if($(window).width() < 780){

		$("[data-horizontal-scroll='mobile']").show();
		$("[data-horizontal-scroll='desktop']").hide();
	}else{

		$("[data-horizontal-scroll='mobile']").hide();
		$("[data-horizontal-scroll='desktop']").show();
	} 



	/*
	################################################
	STICK ELEMENTS
	################################################
	*/ 
	$("#stick-target").hcSticky({
		top: 180,
		bottomEnd: 100,
		responsive: false, 
		offResolutions: [-780]
	}); 


});

$(window).load(function() { 


	
	$("body").removeClass("no-fonts");
}); 