app.factory('windowDimensions', ['$window',
  function($window) {
    return {
      height: function() {
        return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
      },
      width: function() {
        return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      }
    };
  }
]);
 
/*
To do: track people as well througha  cookie - push links they clicked on and time they spent on any page
*/ 
app.service("mpHelper", ["$rootScope", function($rootScope){

    this.dbug = !true;
    this.trackPickForm = function(data){
             
        mixpanel.track("Pick Form Clicks",data); 

        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Pick Form]");
            console.log(data);
            console.log("========================");
        }
    };
 
    this.trackCelebritiesClick = function(data){
             
        mixpanel.track("Celebrities Click",data); 

        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Celebrities Click]");
            console.log(data);
            console.log("========================");
        }
    };

    this.trackZoom = function(data){

        mixpanel.track("Zoom",data); 

        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Zoom]");
            console.log(data);
            console.log("========================");
        }
    }

    this.trackCelebritiesShow = function(data){

        mixpanel.track("Celebrities Show",data); 

        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Celebrities Show]");
            console.log(data);
            console.log("========================");
        }
    } 

    this.trackInfoDropdown = function(data){
             
        mixpanel.track("Info Dropdown",data); 

        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Info Dropdown]");
            console.log(data);
            console.log("========================");
        }
    };

    this.trackAddToCart = function(data){

        mixpanel.track("Add to Cart",data); 
        // fbq('track', 'AddToCart');

        var arr = {};
        var key = "index-";
        for(var product in data.product){
            var i = key+product;
            product = data.product[product];
            arr[i] = product;
        }
        yaCounter35584630.reachGoal('addToCart',arr);
 
        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Add to Cart]");
            console.log(data);
            console.log("========================");
        }
    };

    this.trackCheckout = function(data){
 
        mixpanel.track("Checkout",data);
        // fbq('track', 'Purchase', {value: '0.00', currency: 'RUB'}); 

        yaCounter35584630.reachGoal('checkout',{  
            quantity: data.quantity,
            email: data.email,  
        });

        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Checkout]");
            console.log(data);
            console.log("========================");
        }
    }

    this.trackCompare = function(data){
 
        mixpanel.track("Compare",data); 
 
        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Compare]");
            console.log(data);
            console.log("========================");
        }
    } 

    this.trackSignup = function(data){
 
        mixpanel.track("Signup",data); 
 
        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Signup]");
            console.log(data);
            console.log("========================");
        }
    } 

    this.trackCartView = function(data){
 
        mixpanel.track("Cart View",data); 
        // fbq('track', 'InitiateCheckout');
 
        if(settings.dbug || this.dbug){ 
            console.log("========================");
            console.log("Event [Cart View]");
            console.log(data);
            console.log("========================");
        }
    } 
}]);


app.service("infoHelper", ["$rootScope", "$cookies", "$q", "$http", "$sce", "userHelper", function($rootScope, $cookies, $q, $http, $sce, userHelper){

    this.getMessageHelper = function(response){

        var message = {
            message: response.dynamic, 
            type: "dynamic"
        };
        if(message!==undefined && message!=null && message!="") 
            message = {
                message: response.default, 
                type: "default"
            };
        
        return message;
    }

    this.checkInfo = function(id){


        var self = this;
        var defer = $q.defer(); 

        $http({
            method: 'GET', 
            url: '/wp-json/info/v2/get/info/'+id, 
        }).then(function successCallback(response) {
                   
                response = response.data.data; 

                var resp = self.getMessageHelper(response);  
                var message = resp.message;
                var type = resp.type;
  
                defer.resolve({
                    enable: message.enable,
                    message: $sce.trustAsHtml(message.message),
                    timeout: message.timeout * 1000,
                    type: type
                });

        }, function errorCallback(response) {

            settings.dbug && console.log("error");
            defer.resolve(false);
        });
        return defer.promise;  
    }

    this.dynamicEnabled = function(id){
 
        var cookies = userHelper.get();
        var hasDynamic = cookies.infoDynamic[id];

        return hasDynamic===undefined || hasDynamic; 
    }
    this.defaultEnabled = function(){
 
        var cookies = userHelper.get();
        return cookies.infoDefault;
    }
}]);

app.service("userHelper", ["$rootScope", "$cookies", function($rootScope, $cookies){
       
    this.reset = function(){
        
        var data = {
            instagramFeedHidden : false,
            visitCount : 0,
            pagesViewed : 1,
            promoDropdownHidden : false,
            infoDefault : settings.infoDefault==1,
            infoDynamic : [],
            ad: null,
        };
        // utm_source=instagram //always
        // utm_medium=ad-video | ad-static
        // utm_campaign=Russia-15-45 | Moscow 15-25 and so on
        // utm_term=home-page|compare-page
        // http://3dcrestwhite.ru/?utm_source=instagram&utm_medium=ad-video&utm_campaign=Russia-15-45



        $cookies.putObject("user", data); 
        
        settings.dbug && console.log("######## set #######");
        settings.dbug && console.log(data);
    };
  
    this.get = function(){
        
        var user = $cookies.getObject("user"); 
        if(user===undefined || Object.keys(user).length!=7){

            this.reset(); 
            return this.get();
        }
        
        settings.dbug && console.log("######## get #######");
        settings.dbug && console.log(user);
        return user; 
    }

    this.update = function(key, value){
            
        var data = this.get();
        data[key] = value;
        $cookies.putObject("user", data); 

        settings.dbug && console.log("######## update #######");
        settings.dbug && console.log(data);
    }

    this.updateInfoDefault = function(value, id){
            
        var data = this.get();
        data.infoDefault = value; 
        $cookies.putObject("user", data); 

        settings.dbug && console.log("######## update info default #######");
        settings.dbug && console.log(data);
    }
    this.updateInfoDynamic = function(value, id){
            
        var data = this.get();
        if(data.infoDynamic[id] === undefined)
            data.infoDynamic.push(id); 

        $cookies.putObject("user", data); 

        settings.dbug && console.log("######## update info dynamic #######");
        settings.dbug && console.log(data);
    }

    this.remove = function(key){
             
        settings.dbug && console.log("######## remove #######");
        $cookies.remove("user"); 
    }

    this.pageUp = function(){

        var count = this.get().pagesViewed+1;
        settings.dbug && console.log("######## page count up: "+count+" #######");
        this.update("pagesViewed",count);
    }

    this.setCampaign = function(data){

        var user = this.get();
        settings.dbug && console.log("######## set campaign #######");
        
        // if(user.ad==null)
            this.update("ad",{
                source:data.source,
                medium:data.medium,
                campaign:data.campaign,
                term:data.term, //target of the campaign
            }); 
    }
    this.getCampaign = function(key){

        var user = this.get();
        settings.dbug && console.log("######## get campaign #######");
        
        if(user.ad!=null)
            return user.ad[key];
        return false;
    }

}]);


app.service("otherHelper", ["$q", "$http", function($q, $http, $timeout){
 
    this.getCelebrities = function(product){ 
         
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/other/v2/celebrities',  
        }).then(function successCallback(response) {

            response = response.data.data; 
            settings.dbug && console.log(response); 
            defer.resolve(response);
            
        }, function errorCallback(response) {
  
            settings.dbug && console.log("error");
            defer.resolve(false); 
        });
        return defer.promise;
    }

}]); 

app.service("cartHelper", ["$q", "$http", "$rootScope", "$timeout", "mpHelper", 
    function($q, $http, $rootScope, $timeout, mpHelper){

    this.getTotalPrice = function(product){
 
        var price = product.main.price;
        if(product.fortify!=null)
            price += product.fortify.price;  
        if(product.relief!=null)
            price += product.relief.price;
  
        return price;
    }

    this.celebritiesFolded = true;
    this.checkCelebrities = function(){
        var self = this; 

        var defer = $q.defer();

        var promise = $timeout(function(){
            $("html,body").animate({
                scrollTop:0
            },settings.animationTime,function(){ 
                self.celebritiesFolded = false;  
                defer.resolve(true); 
            });
        },settings.cartTimeout); 

        $("#checkout-form-email input").on("click focus",function(){
            $timeout.cancel(promise);
            defer.resolve(false); 
        });
        if($(window).width() >= 780){
            $timeout.cancel(promise);
            defer.resolve(false); 
        }
 
        return defer.promise;  
    }

    this.addToCart = function(product){

        var defer = $q.defer();
        var promises = [];
        var index = 0; 
        var error = false;

        /*if(product.main!=null)
            promises.push($http({
                method: 'GET',
                url: '/wp-json/cart/v2/add/' + product.main.id + '/' + 1
            }).then(function successCallback(response) {
                index++; 
            }, function errorCallback(response) {
                error = true;
                settings.dbug && console.log("error");
            }));
        
        if(product.fortify!=null)
            promises.push($http({
                method: 'GET',
                url: '/wp-json/cart/v2/add/' + product.fortify.id + '/' + 1
            }).then(function successCallback(response) {
                index++;  
            }, function errorCallback(response) {
                error = true;
                settings.dbug && console.log("error");
            }));

        if(product.relief!=null)
            promises.push($http({
                method: 'GET',
                url: '/wp-json/cart/v2/add/' + product.relief.id + '/' + 1
            }).then(function successCallback(response) {
                index++;  
            }, function errorCallback(response) {
                error = true;
                settings.dbug && console.log("error");
            }));
  
        $q.all(promises).then(function(){
             
            defer.resolve(!error);
        });*/

        //need to nest, woocommerse won't support multiple products adding at once
        if(product.main!=null)
            $http({
                method: 'GET',
                url: '/wp-json/cart/v2/add/' + product.main.id + '/' + 1
            }).then(function successCallback(response) {
                index++; 
                if(product.fortify!=null){

                    $http({
                        method: 'GET',
                        url: '/wp-json/cart/v2/add/' + product.fortify.id + '/' + 1
                    }).then(function successCallback(response) {
                        index++;  
                        if(product.relief!=null){
                            $http({
                                method: 'GET',
                                url: '/wp-json/cart/v2/add/' + product.relief.id + '/' + 1
                            }).then(function successCallback(response) {
                                index++;  
                                defer.resolve(3);
                            }, function errorCallback(response) {
                                error = true;
                                settings.dbug && console.log("error");
                                defer.resolve(-1);
                            });
                        
                        }else{
                            defer.resolve(2);
                        }

                    }, function errorCallback(response) {
                        error = true;
                        settings.dbug && console.log("error");
                        defer.resolve(-1);
                    });

                }else if(product.relief!=null){

                        $http({
                            method: 'GET',
                            url: '/wp-json/cart/v2/add/' + product.relief.id + '/' + 1
                        }).then(function successCallback(response) {
                            index++;  
                            defer.resolve(3);
                        }, function errorCallback(response) {
                            error = true;
                            settings.dbug && console.log("error");
                            defer.resolve(-1);
                        });
                     
                }else{
                    defer.resolve(1);
                }

            }, function errorCallback(response) {
                error = true;
                settings.dbug && console.log("error");
                defer.resolve(-1);
            });
      
        else
            defer.resolve(0);

        return defer.promise;  
    }

    this.addToCartSingle = function(product){

        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/cart/v2/add/' + product.id + '/' + 1
        }).then(function successCallback(response) {
             
            defer.resolve(true);
        }, function errorCallback(response) {

            settings.dbug && console.log("error");
            defer.resolve(false);
        });

        return defer.promise;  
    }

    this.animation = function(elem){

        var $elemOffset = $(elem).offset();
        var $cartOffset = $("#cart").offset(); 

        var $bubble = $("<div class='bubble' id='bubble'></div>");
        $("body").append($bubble);

        $bubble
            .css({
                position:"absolute"
            })
            .offset({
                top: $elemOffset.top,
                left: $elemOffset.left
            })  
            .animate({
                'left': $cartOffset.left,
            },{
                easing: 'easeOutQuad',
                queue: false,
                duration: settings.animationTime
            })
            .animate({
                'top': $cartOffset.top,
            }, settings.animationTime, 'easeInQuad', function(){
                $bubble.animate({
                    'width': 0,
                    'height': 0
                });
                $bubble.remove();
            }); 
 

    }


    this.currentProduct = function(tabs, activeOuterTab, upsell, index){

        var product = {
            main:null,
            fortify:null,
            relief:null,
        };

        if(upsell[0])
            product.fortify = tabs.fortify; 
        if(upsell[1])
            product.relief = tabs.relief; 

        if(activeOuterTab==0)
            product.main = tabs.fast[index];
        if(activeOuterTab==1)
            product.main = tabs.comfortable[index];
        if(activeOuterTab==2)
            product.main = tabs.results[index]; 

        return product;
    } 

    this.cartObjectSet = false;
    this.cartObject = null;
    this.getCart = function(){

        var defer = $q.defer();
        var self = this;

        if(self.cartObjectSet){
            defer.resolve(self.cartObject); 
        }else{
            $rootScope.$watch(function() { 
                return self.cartObject;
            }, function() { 
                if(self.cartObject != null)  {
                    self.cartObjectSet = true;
                    defer.resolve(self.cartObject); 
                }
            }); 
        }
        return defer.promise;  
    }

    this.setCart = function(){
        
        var self = this;
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/cart/v2/get/all'
        }).then(function successCallback(response) {

            response = response.data.data; 
            settings.dbug && console.log(response);

            self.cartObject = {
                quantity: response.quantity,
                total: response.total,
                contents: response.contents 
            } 
            defer.resolve(self.cartObject);
  
        }, function errorCallback(response) {
 
            defer.resolve(false);
        });

        return defer.promise;  
    }
}]);


/*
COntact form service
*/
app.service("emailHelper", ["$q", "$http", "cartHelper", "mpHelper", "userHelper",
                            function($q, $http, cartHelper, mpHelper, userHelper){

    /*
    Delete all products from cart
    */                            
    this.checkoutRemoveProducts = function(){

        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/cart/v2/remove/all/', 
        }).then(function successCallback(response) {
 
            response = response.data.data; 
            settings.dbug && console.log(response);   
            defer.resolve(response);

        }, function errorCallback(response) {
  
            settings.dbug && console.log("error"); 
            defer.resolve(false);
        });
        return defer.promise;
    }

    /*
    Add new customer to db
    */                      
    this.checkoutAddCustomer = function(customer, cartContents){

        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/customers/v2/customer/add/', 
            params: { 
                address : customer.address,
                email : customer.email, 
                phone : customer.phone,  
                name : customer.name,  
                message : cartContents.message,  
                "products[]" : cartContents.products,
            },
        }).then(function successCallback(response) {

            response = response.data.data; 
            settings.dbug && console.log(response); 
            defer.resolve(response);
            
        }, function errorCallback(response) {
  
            settings.dbug && console.log("error");
            defer.resolve(false); 
        });
        return defer.promise;
    }

    /*
    Get checkout data
    */                            
    this.checkoutGetData = function(){

        var defer = $q.defer();
        cartHelper.getCart().then(function(cartContents){

            var products = cartContents.contents; 
            var response = [];
            var quantity = 0; 

            for(var product in products){
                var product = products[product];
                quantity += product.quantity;

                response.push({ 
                    id : product.id,
                    title : product.name, 
                    quantity : product.quantity 
                });
            }   
            defer.resolve({
                products : products,                 
                quantity : quantity, 
            }); 
        });
        return defer.promise;
    }

    this.checkoutSendEmail = function(customer, obj){

        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/emails/v2/checkout/send/', 
            params: {  
                "products[]": obj.products,
                name: customer.name,
                phone: customer.phone,
                email: customer.email,
                address: customer.address,
            },
        }).then(function successCallback(response) {
 
            response = response.data.data; 
            settings.dbug && console.log(response); 
  
            defer.resolve(response);
            
        }, function errorCallback(response) {
  
            settings.dbug && console.log("error");
            defer.resolve(false); 
        });
        return defer.promise;   
    }
                                
    /*
    Get current cart contents, return parsed message
    */
    this.checkoutForm = function(customer){
       
        var defer = $q.defer();
        var self = this;
        self.checkoutGetData().then(function(response){
 
            if(response) { 

                var promises = [
                    self.checkoutAddCustomer(customer, response),
                    self.checkoutSendEmail(customer, response),
                    self.checkoutRemoveProducts()
                ];
                
                var data = {  
                    quantity: response.quantity,
                    email: customer.email,  

                    "source": userHelper.getCampaign("source"),
                    "medium": userHelper.getCampaign("medium"),
                    "campaign": userHelper.getCampaign("campaign"),
                    "term": userHelper.getCampaign("term"),
                }
                
                if(userHelper.getCampaign("source")==""){

                    mpHelper.trackCheckout({  
                        quantity: response.quantity,
                        email: customer.email,   
                    });  
                } else {

                    mpHelper.trackCheckout(data);  
                }
         
                $q.all(promises).then(function(){ 

                    defer.resolve(data);
                }); 
            }else 
                defer.resolve(false);
        });  

        return defer.promise; 
    }

    /*
    Final order message
    */
    // this.checkoutForm = function(){

    //     var defer = $q.defer();
    //     var self = this;
    //     var $form = $("#checkout-form-email form"); 
    //     var $button = $form.find("input[type='submit']");  
        
    //     var classes = $button.attr("class"); 
    //     var value = $button.val(); 

    //     $button.after("<button id='checkout-button' class='"+classes+"'><span 3d-button><span class='text'>"+value+"</span> <div class='cs-loader loading'> <div class='cs-loader-inner'> <label> ●</label> <label> ●</label> <label> ●</label> <label> ●</label> <label> ●</label> <label> ●</label> </div> </div> </span></button>");
    //     $button.remove();  
    //     $button = $("#checkout-button");
          
    //     $form.submit(function(event) {  
    //         event.preventDefault();

    //         $button.addClass("active");
 

    //         if(!$form.find('.wpcf7-mail-sent-ok').length){
    //             $button.removeClass("active");
    //         } else { 
                
    //             self.checkoutFormHelper().then(function(cartContents){
 
    //                 /*
    //                 Parse message for html email template
    //                 */      
    //                 var textarea = $form.find("textarea[name='your-message']");
    //                 textarea.html(cartContents.message); 

    //                 var promises = [];
    //                 var index = 0; 

    //                 /*
    //                 Push request to remove all cart objects
    //                 */ 
    //                 promises.push($http({
    //                     method: 'GET',
    //                     url: '/wp-json/cart/v2/remove/all/', 
    //                 }).then(function successCallback(response) {

    //                     index++; 
    //                     response = response.data.data; 
    //                     settings.dbug && console.log(response); 
                        
    //                 }, function errorCallback(response) {
              
    //                     settings.dbug && console.log("error"); 
    //                 })); 

    //                 /*
    //                 Push request to add new customer
    //                 */
    //                 promises.push($http({
    //                     method: 'GET',
    //                     url: '/wp-json/customers/v2/customer/add/', 
    //                     params: { 
    //                         address : $form.find('.your-address textarea').val(),
    //                         email : $form.find("input[name='your-email']").val(), 
    //                         phone : $form.find("input[name='your-phone']").val(),  
    //                         name : $form.find("input[name='your-name']").val(),  
    //                         message : cartContents.message,  
    //                         products : [cartContents.products],
    //                     },
    //                 }).then(function successCallback(response) {

    //                     index++; 
    //                     response = response.data.data; 
    //                     settings.dbug && console.log(response); 
                        
    //                 }, function errorCallback(response) {
              
    //                     settings.dbug && console.log("error"); 
    //                 }));
                    
    //                 /*
    //                 Defer with true if both requests succeeded
    //                 */
    //                 $q.all(promises).then(function(){ 

    //                     defer.resolve(index==2);
    //                 }); 
    //             });
    //         } 
    //     });

    //     return defer.promise; 
    // }


}]);


 

app.service("reviewHelper", ["$q", "$http", "$rootScope", function($q, $http, $rootScope){
     
    this.getCurrentReview = function(id){  
    
        var self = this;  
        //show loader
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: '/wp-json/instagram/v2/live/get/'+id 
        }).then(function successCallback(response) {

            response = response.data.data; 
            settings.dbug && console.log(response);  
            defer.resolve(response);

        }, function errorCallback(response) {
  
            settings.dbug && console.log("error");
            defer.resolve(false);
        });
  
        return defer.promise;
    }


    this.getReviewsAll = function(){
        
        var defer = $q.defer();
        var self = this;  
        $http({
            method: 'GET',
            url: '/wp-json/instagram/v2/live/get/all', 
        }).then(function successCallback(response) {
  
            defer.resolve(response.data.data);
         
        }, function errorCallback(response) {

            settings.dbug && console.log("error"); 
            defer.resolve(false);
        }); 
        return defer.promise; 
    }; 
}]);