<?php 
/**
 * The template for displaying all pages.
 * Template name:Instruction
 * This is the template that displays the contact us page
 *
 * @package web2feel
 */ 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;
include 'templates/pages/instruction.php';
?>
