<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}
	
	$title = get_the_title(); 

	$image_main_full = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); 
	$image_main_full = $image_main_full[0];
	$image_main = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); 
	$image_main = $image_main[0];

	$image_main_medium = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); 
	$image_main_medium = $image_main_medium[0];

	$regular_price = get_post_meta( get_the_ID(), '_regular_price');
	$sale_price = get_post_meta( get_the_ID(), '_sale_price');

	get_header( 'shop' ); 
?>  
<main class="single" ng-controller="singleController" ng-click="navigationTrigger(-1)"> 

	<section ng-controller="zoomController" class="single-head clear" id="single-head">
		<div class="single-head-wrap">

	 		<!-- left start -->
			<div class="image-wrap" id="zoom-original" ng-mouseenter="overviewHide('<?php echo $title; ?>')" ng-mouseleave="overviewShow()">	
 
				<img src="<?php echo $image_main_medium; ?>" src-large="<?php echo $image_main; ?>" class="zoom-image" alt="<?php echo $title; ?>">  
	  
				<?php     
					/*foreach ($product->get_gallery_attachment_ids() as $key => $value) : 
		  
						$image_link = wp_get_attachment_url( $value,'large' );
						$image_link_full = wp_get_attachment_url( $value,'full',false ); 
				?> 							 
					<a class="img-url" href="<?php echo $image_link_full; ?>">
						<?php echo wp_get_attachment_image( $value,'large'); ?>
					</a>
				<?php endforeach; */?> 

			</div>
	 		<!-- left end -->

	 		<!-- right start -->
			<div class="box-wrap">
	 
				<div class="zoom-zoomed hide" id="zoom-zoomed">
					<img class="zoom-image">  
				</div>

				<div class="overview" ng-class="getClass()">

				 	<h1 class="title"><?php echo $title; ?></h1>

					<p class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">  
		 
						<?php if($sale_price[0] != '') : ?>
							<span class="after calibrate"><?php echo $sale_price[0]; ?> <i class="fa fa-rub"></i></span> 
							<span class="before"><?php echo $regular_price[0]; ?> руб.</span>
						<?php else: ?>
							<span class="after"><?php echo $regular_price[0]; ?> руб.</span>  
						<?php endif; ?>

					</p>
					<?php if($sale_price[0] != ''): ?>
						<h4 class="sale">распродажа</h4>  
					<?php endif; ?>
					
					<div class="form">  
						<?php while ( have_posts() ) : the_post(); ?> 
							<?php 
								/**
								 * woocommerce_single_product_summary hook
								 *
								 * @hooked woocommerce_template_single_title - 5
								 * @hooked woocommerce_template_single_rating - 10
								 * @hooked woocommerce_template_single_price - 10
								 * @hooked woocommerce_template_single_excerpt - 20
								 * @hooked woocommerce_template_single_add_to_cart - 30
								 * @hooked woocommerce_template_single_meta - 40
								 * @hooked woocommerce_template_single_sharing - 50
								 */
								do_action( 'woocommerce_single_product_summary' );
							?> 
						<?php endwhile; // end of the loop. ?> 

					</div>
				</div>
			</div>
	 		<!-- right end -->

		</div> 
	</section>
	  
	<?php /*$video = get_field('product_video');   if($video != '' && is_set($video) && false):  ?>
	<section class="flexslider video"> 
		<div class="coverup" id="flexslider">  
		<?php if(sizeof($video) == 1) : ?>
			<div class='embed-responsive embed-responsive-16by9'>
				<?php the_video($video[0]); ?> 
			</div>
		<?php else: ?>
			<ul class="slides">  
				<?php foreach ($video as $key => $value) :  ?>
					<li>
						<?php the_video($value); ?> 
					</li>
				<?php endforeach; ?> 
			</ul>
		<?php endif; ?>	
		</div> 
	</section>
	<?php endif;*/ ?>

	<?php	
	$reviews = get_posts(array(
		'post_type' => 'reviews',
		'posts_per_page'   => 5,
		'meta_query' => array(
			array(
				'key' => 'reviews_product', // name of custom field
				'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
		)
	));   
	?>

	<article class="maintext">
		<div class="wrapper"> 
			<section class="content-wrap">
				<div class="content-tab-links">
					<span class="tab-link" ng-click="tabChange(0)" ng-class="tabIsActive(0)">
						Описание
					</span>
					<?php if($reviews): ?>
						<span class="tab-link" ng-click="tabChange(1)" ng-class="tabIsActive(1)">
							Отзывы на <?php echo get_field("product_short_title", get_the_ID()); ?>
						</span>
					<?php endif; ?>
					<span class="tab-link" ng-click="tabChange(2)" ng-class="tabIsActive(2)">
						ЧаВО  
					</span>
				</div>
				<div class="content-tabs">
					
					<div class="tab content" ng-class="tabIsActive(0)">
						<?php the_content(); ?>
					</div>
					<?php if($reviews): ?>
						<div class="tab reviews" ng-class="tabIsActive(1)"> 
							<?php
							foreach ($reviews as $key => $value) : ?>
							   	<div class="review">
							   		<div class="left">
							   			<img src="<?php Helpers::theImage("medium", $value->ID); ?>">
							   		</div>
							   		<div class="right">
								   		<h3 class="title"><?php echo $value->post_title; ?></h3>
								   		<div class="content"><?php echo $value->post_content; ?></div>
								   		<a href="<?php echo get_field("reviews_link",$value->ID); ?>" class="review-link" target="_blank">Ссылка на отзыв</a>
							   		</div>
							   	</div>
							<?php endforeach; ?> 
						</div>
					<?php endif; ?>
					<div class="tab faq" ng-class="tabIsActive(2)">
						<?php
						$query = new WP_Query(array(
						    'post_type' => 'faq-posts', 
						    'posts_per_page' => -1,
						)); 
						$helpers = new helpers();
						$counter = 0; 	
						if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 

								$post = get_post();
						    	$title = $post->post_title; 
						    	$content = apply_filters('the_content', $post->post_content);
						?>
						<div class="faq-slide" ng-click="faqShow(<?php echo $counter; ?>)" ng-class="faqIsActive(<?php echo $counter; ?>)"> 
					   		<h3 class="title"><?php echo $title; ?></h3>
					   		<div class="content"><?php echo $content; ?></div>
						</div>

						<?php $counter++; endwhile; endif; ?>
					</div>
				</div>
			</section>
			<section class="sidebar-tiles" id="stick-target">
				<?php include get_template_directory().'/templates/other/sidebar-tiles-celebrities.php'; ?>
				<?php include get_template_directory().'/templates/other/sidebar-attributes.php'; ?>
			</section>
		</div>
	</article>  
    
	<section class="horizontal-scroll-wrap" ng-controller="loopController" data-horizontal-scroll="mobile"> 

		<h2>Самые популярные полоски</h2>

		<div class="scrollbar">
			<div class="handle">
				<div class="mousearea"></div>
			</div>
		</div>
 
		<div class="clear">
			<div class="sly-nav prev">
				<i class="fa fa-chevron-left" aria-hidden="true"></i>
				<span class="text">Предыдущий</span>
			</div>
			<div class="sly-nav next">
				<span class="text">Следующий</span>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</div>
		</div>

		<div class="frame" id="horizontal-products">
			<div class="clearfix"> 
				<?php 
				$args = array(  
				    'post_type'  	 => 'product',  
				    'meta_key'   	 => '_featured',  
				    'meta_value' 	 => 'yes',  
	                'posts_per_page' => -1,
	                'orderby'        => 'menu_order'  
				);  
				  
				$query = new WP_Query( $args );  
				      
				if ($query->have_posts()) : 
					while ($query->have_posts()) : $query->the_post();  
						wc_get_template_part( 'content', 'product' );   
				    endwhile;   
				    wp_reset_postdata();
				endif;    
			 	?> 
			</div>
		</div>
	</section>

	<div class="products" ng-controller="loopController" data-horizontal-scroll="desktop"> 
		<div class="woocommerce">
			<div class="js-masonry" id="js-masonry" >
		 	<?php
		 	   
			$args = array(  
			    'post_type'  	 => 'product',  
			    'meta_key'   	 => '_featured',  
			    'meta_value' 	 => 'yes',  
                'posts_per_page' => -1,
                'orderby'        => 'menu_order' 
			);  
			  
			$query = new WP_Query( $args );  
			      
			if ($query->have_posts()) : 
			while ($query->have_posts()) : $query->the_post();  
				wc_get_template_part( 'content', 'product' );   
		    endwhile;   
			endif;  
			  
			wp_reset_query();  
		 	?>
			</div>
		</div>
	</div>

</main> 
<?php get_footer( 'shop' ); ?>
