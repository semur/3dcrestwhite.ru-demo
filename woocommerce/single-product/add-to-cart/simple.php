<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

?>

<?php
	// Availability
	$availability      = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';
	
	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>
<input type="hidden" value="<?php echo $available;?>" id="available">

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" method="post" enctype='multipart/form-data'>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
		<div class="quantity-wrap">
			 
		 	<div>
				<span>Количество</span> 
				<h4 class="quantity-prompt">при покупке в 2 и более прилагаются скидки</h4> 
			</div>
			<?php
		 		if ( ! $product->is_sold_individually() )
		 			woocommerce_quantity_input( array(
		 				'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
		 				'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
		 			) );
		 	?>  
		 	<?php /* ?><div class="quantity-discounts">
		 		<?php 

				$regular_price = get_post_meta( get_the_ID(), '_regular_price');
				$sale_price = get_post_meta( get_the_ID(), '_sale_price');
				$single_price = $regular_price[0];
				if($sale_price[0] != '') 
					$single_price = $sale_price[0]; 
 
		 		$prices = get_field("product_prices");
		 		if(!empty($prices)) :
		 		?> 
					<div>
					    <span>1</span>
					    <span><?php echo $single_price; ?> <i class='fa fa-ruble'></i></span> 
					</div>
		 		<?php
		 		foreach ($prices as $key => $value) : 
		 		?> 
					<div>
					    <span><?php echo $value['product_prices_quantity']; ?></span>
					    <span><?php echo $value['product_prices_pricce']; ?> <i class='fa fa-ruble'></i></span> 
					</div>
		 		<?php
		 		endforeach;
		 		endif;
		 		?>
			</div> <?php */ ?>
		</div>
	 	
	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
 	
 		<?php  
		$hp = new helpers();  
		$productSummary = (object)array(
			"id" => $product->post->ID,
			"title" => $hp->get_short_title($product->post),
			"price" => $product->price,
		);
		$productSummary = json_encode($productSummary);
		?>  
	 	<button class="submit add-to-cart-button" ng-click='addToCart($event, <?php echo $productSummary; ?>)' ng-class="{active: cartIsLoading}">
	 		<span button-text="добавить в корзину" 3d-button></span> 
		</button>
	 	
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?> 
 

<?php /* ?><span class="share-prompt">
	Тебе понравились <?php echo the_title(); ?>? Расскажи о нем друзьям!
</span>
<div class="single-share-buttons">
    <div class="view-count" id="view-count"></div>
    <div class="view-count-slash"></div>
	<div class="fb-button">
            
        <i class="fa fa-facebook-official"></i>
		<font>Поделиться</font> 
	</div>
                
    <a  class="vk-button" href="http://vkontakte.ru/share.php?url=http://yvek.ru/интересное/автомобиль-тесла-характеристики/" target="_blank" > 

        <i class="fa fa-vk"></i>
		<font>Поделиться</font> 
	</a>
</div> <?php */ ?>