<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
	
	<div class="breadcrumbs">
		<div class="cap">
			<?php echo woocommerce_breadcrumb(); ?>
		</div>
	</div> 
	
	<main class="products" ng-click="navigationTrigger(-1)">


		<div class="custom-shortcode" data-effect="left">
	        <div class="wi-center-heading has_boder">
	            <h1 class="h">Сэкономь время и нервы на визитах к врачу</h1>
	            <div class="subtitle">Отбеливающие полоски обеспечат быстрый и безопасный результат</div>
	        </div>
	        <div class="viewport-ele"></div>
		</div> 
		  
		<!-- masonry start -->
		<?php if ( have_posts() ) : ?> 

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>
	 

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?> 
		<!-- masonry end -->
	</main>
	 
<?php get_footer( 'shop' ); ?>
