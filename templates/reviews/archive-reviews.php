<?php
/** 
 * @package web2feel
 */ 
get_header(); 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;
?> 
<main class="reviews" ng-click="navigationTrigger(-1)"> 

	<section class="header cap">
	    <div class="left">
	        <div class="heading clear">  
                <h1>
				    <span class="big"><?php the_title(); ?></span>  
				</h1>  
				<div>
					<?php the_content(); ?>
				</div>
	        </div> 
	    </div>
	    <div class="right">
			<?php echo get_sidebar(); ?> 
	    </div>
	</section>

	<section class="posts"> 
        <div class="wrapper clear">
        	<div class="content"> 
	        	<?php
				$query = new WP_Query(array(
					'posts_per_page'   => 10, 
					'post_type'        => 'reviews',
					'paged'   		   => get_query_var('paged') ? get_query_var('paged') : 1, 
					'post_status'      => 'publish', 
				));   
				if ($query->have_posts()) : ?>
					<?php while ($query->have_posts()) : $query->the_post(); ?>
				    	<article>  
					   		<div class="left">
					   			<img src="<?php Helpers::theImage("medium", get_the_ID()); ?>">
					   		</div>
					   		<div class="right">
						   		<h2 class="title"><?php the_title(); ?></h2>
						   		<div class="content"><?php the_content(); ?></div>
						   		<a href="<?php echo get_field("reviews_link",$value->ID); ?>" class="review-link" target="_blank">Ссылка на отзыв</a>
					   		</div> 
				    	</article>   
					<?php endwhile; ?>				
 
					<div class="archive-navigation">
						<div class="nav prev">
							<?php echo previous_posts_link('Назад'); ?>
						</div>
						<div class="nav next">
							<?php echo next_posts_link('Вперед', $query->max_num_pages); ?>				
						</div>  
					</div> 

				<?php else: ?> 
					<article>
						Ошибка 404 Not Found (страница не найдена)...
					</article>
				<?php endif; ?>  
			</div>
        	<div class="sidebar-tiles" id="stick-target">   
				<?php include get_template_directory().'/templates/other/sidebar-tiles-celebrities.php'; ?>
				<?php include get_template_directory().'/templates/other/sidebar-attributes.php'; ?>
        	</div>
        </div>
	</section>
</main> 
<?php get_footer(); ?>  