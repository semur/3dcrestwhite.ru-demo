<aside class="instagram-feed-live-popup popup" ng-controller="liveReviewsControllerSingle" ng-show="displayReviewSingle()" ng-cloak> 
 
    <div class="top clear">
        <span class="text">Отзывы наших клиентов</span>
        <span class="close" ng-click="hideReview()">X</span> 
    </div>
 
    <div ng-show="displayReviewLoading()"> 
        <!-- loader start -->
        <div class="cs-loader loading" ng-hide="formIsLoaded()">
            <div class="cs-loader-inner">
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
                <label> ●</label>
            </div>
        </div> 
        <!-- loader end -->
    </div>

    <div class="bottom clear" ng-hide="displayReviewLoading()"> 

        <div class="left"> 
            <img ng-src="{{review.image}}">
        </div>
        <div class="right">

            <div class="top clear">     
                <div class="left">
                    <div class="circle crop">
                        <img src="https://scontent-dfw1-1.cdninstagram.com/t51.2885-19/s150x150/12142511_868893046557198_1595857515_a.jpg">
                    </div>
                </div>
                <div class="right">
                    <a href="{{review.link}}">@3dcrestwhite.ru</a> 
                </div> 
            </div>

            <div class="stats clear"> 
                <span class="likes">
                    <i class="fa fa-heart"></i>
                    <span id="lightbox-likes">{{review.likes_count}}</span> 
                    <span>лайков</span>
                </span>
                <span class="date"></span>
            </div>

            <div class="middle clear">  
                <div>
                    <span class="user">@3dcrestwhite.ru</span>
                    <p>{{review.caption}}</p>
                </div>      
                <div ng-repeat="r in review.commenters">
                    <span class="user">@{{r.from.username}}</span>
                    <p>{{r.text}}</p>
                </div>             
            </div>

            <div class="bottom">  
                <a class="button-link clear" href="{{review.link}}" target="_blank">
                    <span class="text">Подпишись для скидок и промо акции</span>
                    <f class="fa fa-instagram"></f>
                </a> 
            </div>


        </div>
    </div>
</aside>

