<section class="instagram-feed-live" ng-controller="liveReviewsControllerArchive" ng-class="isFolded()"> 
	
	<h2>
		Что о нас говорят
	</h2>

	<div class="cs-loader loading" ng-hide="displayReviewsAll()" ng-cloak>
		<div class="cs-loader-inner">
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
			<label>	●</label>
		</div>
	</div> 
    <div class="cap" ng-show="displayReviewsAll()" ng-cloak>
 		<masonry class="masonry-center" id="instagram-feed-live-scroller" reload-on-resize>
	        <article ng-repeat="item in getReviewsAll" ng-click="showReview($event, $index)" class="masonry-brick">
	            <a href="{{item.link}}" target="_blank">
	                <div class="crop">
	                    <img width="240" height="240" ng-src="{{item.image_small}}">
	                </div> 
	                <div class="wrap">
	                    <div class="wrap-info">
	                        
	                        <div>
	                            <i class="fa fa-heart"></i>
	                            <span>{{item.likes_count}}</span>
	                        </div>
	                        <div>
	                            <i class="fa fa-envelope"></i> 
	                            <span>{{item.comments_count}}</span>
	                        </div>

	                    </div>
	                    <p> 
	                        {{item.caption}}
	                    </p>
	                </div>
	            </a>
	        </article> 
	    </masonry>
    </div>
    <div class="unfold" ng-click="changeFold()" ng-hide="!isFolded()">
    	развернуть <i class="fa fa-angle-double-down" aria-hidden="true"></i>
    </div>
</section>