<!-- navigation start -->
<nav class="navigation">
	<ul class="wrap">
		
		<?php  
			$menu = wp_nav_menu(array(
			    'menu'            => 'Main Menu',
			    'container'       => false,
			    'container_class' => '',
			    'container_id'    => '',
			    'menu_class'      => '',
			    'menu_id'         => '',
			    'echo'            => true, 
			    'before'          => '',
			    'after'           => '', 
			    'items_wrap'      => '%3$s',
			    'link_before'     => '<span>',
			    'link_after'      => '</span>',
			    'depth'           => 0,
			    'walker'          => new WalkerAngular
			));  
		?>   

		<li class="menu-item callback-wrap menu-item-has-children">
			<span class="link callback-trigger" ng-click="navigationTrigger(100)">
				<span><?php echo get_field("global_phone","options"); ?></span>
			</span>
			<ul class="sub-menu callback-dropdown rightmost" ng-show="navigationDropdown(100)" ng-cloak>
				<li class="menu-item callback-call-us clear">
					<a href="tel:456789">
						<span class="text">Наш телефон:</span>
						<span class="phone"><?php echo get_field("global_phone","options"); ?></span>
					</a>
				</li>
				<li class="menu-item callback-email-us">
					<a href="<?php echo get_permalink(684); ?>">
						<span>Написать нам E-mail</span> 
					</a>
				</li>

				<li class="menu-item callback-form">  
					<?php include get_template_directory().'/templates/forms/callback-form.php'; ?>    
				</li> 
			</ul>
		</li>
		<!-- buttona t checkout - option to like our page for a discount of 5% -->
		
	</ul> 
</nav>
<!-- navigation end --> 