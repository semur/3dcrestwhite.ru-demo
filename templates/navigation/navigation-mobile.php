<!-- mmenu start -->
<nav id="mmenu" ng-cloak>
	<ul> 
		<li>
			<a class="link" href="<?php echo wc()->cart->get_cart_url(); ?>">
				<i class="fa fa-shopping-cart"></i>
				<span>Корзина</span>
			</a>
		</li>	
		<?php 
			$menu = wp_nav_menu(array(
			    'menu'            => 'Main Menu',
			    'container'       => false,
			    'container_class' => '',
			    'container_id'    => '',
			    'menu_class'      => '',
			    'menu_id'         => '',
			    'echo'            => true, 
			    'before'          => '',
			    'after'           => '', 
			    'items_wrap'      => '%3$s',
			    'link_before'     => '<span>',
			    'link_after'      => '</span>',
			    'depth'           => 0,
			    'walker'          => new MobileWalker
			));  
		?> 
		<li>
			<a class="link" href="tel:<?php echo get_field("global_phone","options"); ?>">
				<i class="fa fa-phone"></i>
				<span>
					<?php echo get_field("global_phone","options"); ?>
				</span>
			</a>
		</li>
	</ul>
</nav>  
<!-- mmenu end -->