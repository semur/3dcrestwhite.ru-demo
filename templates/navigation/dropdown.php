<div ng-controller="infoController" pageid="<?php echo get_the_ID(); ?>" ng-cloak>

	<!-- promotion start -->
	<div class="promotion" ng-show="promotionIsActive()" >
		<div class="cap">
			<span>{{ promotion }}</span>

			<div class="close" ng-click="promotionClose()">X</div>
		</div>
	</div>
	<!-- promotion end -->

	<!-- promotion start -->
	<div class="info" ng-show="infoIsActive()" >
		<div class="cap">
			<span ng-bind-html="info"></span> 
			<div class="close" ng-click="infoClose()">X</div>
		</div>
	</div>
	<!-- promotion end -->

</div>