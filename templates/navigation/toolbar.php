<!-- toolbar desktop start -->  
<div class="toolbar">
	<div class="wrap">
		<div class="mmenu-trigger" id="mmenu-trigger">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="left">
			<a href="/"> 
				<?php echo wp_get_attachment_image(1283,"large",false,array("class"=>"logo", "alt"=>get_bloginfo())); ?>
			</a>
		</div> 
		<div class="right" ng-class="showIsActive()" ng-cloak> 
			<a class="cart" id="cart" href="<?php echo wc()->cart->get_cart_url(); ?>">
				<span class="text">корзина</span>
				<span class="bubble">{{quantity}}</span>
			</a>
		</div>
	</div>
</div>
<!-- toolbar desktop end --> 