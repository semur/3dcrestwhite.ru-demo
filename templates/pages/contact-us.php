<?php
/** 
 * @package web2feel
 */ 
get_header(); 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;
?> 
<main class="contact-us" ng-click="navigationTrigger(-1)"> 

	<section class="header cap">
	    <div class="left">
	        <div class="heading clear"> 
	            <div class="right">
	                <h1>
					    <span class="big"><?php the_title(); ?></span> 
					    <span class="small">Мы ответим вам в ближайшие 24 часа</span>
					</h1> 
	            </div>
	        </div> 
	    </div>
	    <div class="right">
			<?php echo get_sidebar(); ?> 
	    </div>
	</section>

	<article class="content clear"> 
        <div class="cap">
            <?php the_content(); ?>
        </div>
	</article>
</main> 
<?php get_footer(); ?>

