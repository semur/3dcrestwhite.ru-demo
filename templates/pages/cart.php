<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); 
?>  
<main id="content" class="content cart clear" ng-controller="cartController" ng-click="navigationTrigger(-1)"> 
	<div class="cap"> 
		<div class="custom-shortcode" data-effect="left" ng-hide="checkoutFormShow()">
		    <div class="wi-center-heading has_boder">
		        <h2 class="h">оформление заказа</h2>
		        <div class="subtitle">заполните форму и мы объясним вам процедуру оплаты и доставки</div>
		    </div>
		    <div class="viewport-ele"></div>
		</div>

		<!-- celebrities start -->
		<section class="sidebar-tiles mobile-tiles" ng-class="showCelebrities()" ng-cloak>
			<?php include get_template_directory().'/templates/other/sidebar-tiles-celebrities.php'; ?>
		</section>
		<!-- celebrities end -->

		<!-- tiles start -->
		<section ng-hide="checkoutFormShow()">
			<?php include get_template_directory().'/templates/other/cart-tiles.php'; ?> 
		</section> 
		<!-- tiles end -->

		<!-- promo start -->
		<div class="popup-link" ng-hide="promptShow()" ng-cloak>
			<div class="overlay"></div>
			<img src="<?php echo get_template_directory_uri(); ?>/public/img/cart/promos/promo-1.png">

			<div class="submit-wrap"> 
				<h2> 
					Спасибо за ваш заказ! 
					<br>
					Мы с вами свяжемся <i class="fa fa-smile-o" aria-hidden="true"></i> 
				</h2>
				<span class="text">
					<?php echo get_field("global_checkout_prompt","options"); ?>
				</span> 
				<span class="ig-follow" data-id="b16ec9f3cf" data-handle="3dcrestwhite.ru" data-count="true" data-size="large" data-username="true" ng-click="signUp()"></span> 

				<a class="submit" href="<?php echo get_field("global_instagram_link","options"); ?>">
					Наша страница на Instagram
				</a>  
				
				<div class="shadow"></div>
			</div>
		</div>
		<!-- promo end -->

		<!-- form start -->
		<div class="clear"ng-hide="checkoutFormShow()">
			<?php include get_template_directory().'/templates/forms/cart-form.php'; ?> 

			<section class="sidebar-tiles" id="stick-target" ng-cloak>
				<?php include get_template_directory().'/templates/other/sidebar-tiles-celebrities.php'; ?> 
			</section>
		</div>
		<!-- form end -->
	</div>
</main>

<?php echo get_field("instagram_follow_library","options"); ?>

<?php get_footer(); ?>