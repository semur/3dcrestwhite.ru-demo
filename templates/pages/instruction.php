<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * The template for displaying all pages. * This is the template that displays the contact us page
 *
 * @package web2feel
 */ 
get_header(); 
global $post;
?> 
<main class="instructions" ng-click="navigationTrigger(-1)">
	<section class="video">
		<iframe height="380" src="https://www.youtube.com/embed/R5RV1swdlZ0" frameborder="0" allowfullscreen></iframe>
	</section> 

	<section class="header cap">
	    <div class="left">
	        <div class="heading clear"> 
	            <div class="right">
	                <h1>
					    <span class="big">инструкция </span>
					    <span class="small"><?php the_title(); ?></span>
					</h1>
	                <p>
	                    <?php the_content(); ?>
	                </p>
	            </div>
	        </div> 
	    </div>
	    <div class="right">
			<?php echo get_sidebar(); ?> 
	    </div>
	</section>

	<section class="content clear">
		<div class="left">
			<!--start wrap--> 
			<div class="wrap">

				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="прочитайте инструкции на коробке отбеливающих полосок 3D Crest White" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-1.jpg" >
				    </div> 
				    <div class="text">
				        <h2> 
							прочитайте инструкции на коробке 
						</h2>
				        <h3>
							Серьезно. бесполезных инструкций не бывает
						</h3> 
				        <p>
				            К каждому бренду отбеливающих полосок прилагаются отдельные инструкции. 
				            <br>
				            Внимательно следуйте всем пунктам для избежания повреждения эмали ваших зубов.
				        	<br>
				        	Если у вас есть вопросы, у нас уже наверняка есть на них ответы на странице 
				        	<?php $type = get_post_type_object('faq-posts'); ?>
	                        <a href="/<?php echo $type->rewrite['slug']; ?>">
				        	часто задаваемых вопросов 
	                        </a>
				        </p>
				    </div>
				</div> 
			    <div class="number-bubble">1</div>
			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Начните с отбеливающие полоски для верхних зубов" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-2.jpg" >
				    </div> 
				    <div class="text">
				        <h2>
							Начните с полоски для верхних зубов
						</h2>
				        <h3>
							Верхние зубы обычно самые заметки, особенно при улыбке
						</h3> 
				        <p>
				        	Некоторые бренды, включая 3DCrestWhite, выпускают полоски разных размеров для верхних и нижних зубов. 
				            <br>
				        	Полоска большого размера - для ваших верхних зубов. 
				        	Узкие полоски - для нижних.
				        </p>
				    </div>
				</div> 
			    <div class="number-bubble">3</div> 

			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Отделите упаковку от каждой пленки 3D Crest White" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-3.jpg" >
				    </div> 
				    <div class="text">
				        <h2> 
							Отделите упаковку от каждой пленки
						</h2>
				        <h3>
				        	Постарайтесь не трогать часть полоски которая клеится на зубы
						</h3> 
				        <p>  
							Потяните полоску аккуратно на себя не помяв ее, что бы полоска оставалась
							прямой когда вы будете наклеивать ее на зубы.  
							<br>
							Все равно что наклеить пластырь, не скомкая его в процессе.  
				        </p>
				    </div>
				</div>
			    <div class="number-bubble">5</div> 

			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Отделите полоски 3D Crest White от ваших зубов" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-8.jpg" >
				    </div> 
				    <div class="text">
				        <h2>
							Отделите полоски от ваших зубов 
						</h2> 
						<h3> 
							Чистыми руками, разумеется 
						</h3> 
				        <p>
				        	Чистыми руками, отделите полоску двумя пальцами от зубов после количества минут указанных на коробке.
				            (возможно не получится с первого раза). 
				            <br>
				        	Подождите пока руки полностью не высохнут для более удобного охвата полоски пальцами.  
				            <br>
				        	Либо сверьтесь с <a href="<?php echo get_post(381)->uri; ?>">таблицей</a> которая 
				            <br>
				        	сравнит все типы отбеливающих полосок 3D Crest White по эффективности отбеливания и срока лечения.
				        </p>
				    </div>
				</div>

			    <div class="number-bubble">7</div> 

			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="повторяйте процедуру отбеливания каждый день в течении длительности лечения" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-10.jpg" >
				    </div> 
				    <div class="text">
				        <h2>
							повторяйте процедуру каждый день в течении длительности лечения
						</h2>
				        <h3> 
				        	Перерывы в лечении скажутся на результатах
						</h3> 
				        <p>  
				        	Некоторые бренды обещают результаты в ближайшие 3 дня. Не расстраивайтесь,  
							если вы не заметите разницы в оттенке вашей улыбки.
							<br>
							Помните, что процесс отбеливания требует терпения.
							<br>
							Также не забывайте чистить зубы и пользоваться ополаскивателем в сочетании с полосками -
							<br> 
							полоски 3D Crest White eto не замена, а дополнение к уходу за вашими зубами для достижения
							улыбки способной дать фору голливудским звездам и их стоматологам.  
				        </p>
				    </div>
				</div>
			    <div class="number-bubble">9</div> 

			</div> 
			<!--end wrap--> 

		</div>
		<div class="right">
	  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Поместите полоску 3D Crest White стороной с гелием на внешнюю сторону ваших зубов" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-4.jpg" >
				    </div> 
				    <div class="text">
				        <h2> 
				        	Поместите полоску стороной с гелием на внешнюю сторону ваших зубов
						</h2>
				        <h3> 
							Распрямите полоску и приложите плотно на ваши зубы
						</h3> 
				        <p>
				        	Применять полоски в первый раз может быть сложно.
				        	<br>
				        	При необходимости, вы можете использовать зубную щетку чтобы распрямить склеенные части полоски на зубах. 
				        	<br>
				        	Таким образом вы избежите неравномерного отбеливания.  
				        </p>
				    </div>  
				</div> 
			    <div class="number-bubble">2</div>

			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Повторите процесс с нижней полочкой 3D Crest White" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-5.jpg" >
				    </div> 
				    <div class="text">
				        <h2>
				        	Повторите процесс с нижней полочкой
						</h2>
				        <h3>
				        	Убедитесь что вы используйте более узкие полоски для нижних зубов
						</h3> 
				        <p> 
							Если вы хотите, покрыть большую поверхность нижних зубов, вы можете использовать полоску для верхних зубов. 
							Главное помните что у вас могут закончится верхние полоски раньше чем нижние.
				        </p>
				    </div>

				</div> 
			    <div class="number-bubble">4</div>

			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Ждите указанное на 3D Crest White коробке время" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-6.jpg" >
				    </div> 
				    <div class="text">
				        <h2>
				        	Ждите указанное на коробке время
						</h2>
				        <h3>
				        	Используйте более узкой полоской для нижних зубов
						</h3> 
				        <p> 
				        	Убедитесь что гел полностью просочиться через емаль.
				        	<br>
				        	Постарайтесь не говорит или сильно двигать губами иначе пластинки сдвинуться или полностью отклеются. 
				        	<br> 
				         	Так же постарайтесь сильно не двигать языком чтобы гель не распространился по вашему рту.
				        	<br> 
				         	Распределите язык между зубами чтобы нижняя полоска не была повреждена / сдвинута верхними зубами. 
				        	<br>  
				        	Не оставляйте полоски на зубах дольше, чем рекомендуется,
				        	и с большинством брендом не рекомендуется использовать их дольше чем 30 минут.
				        	<br>  
				        	Длительное применение может привести к повышенной чувствительности зубов и десен. 
				        	<br>  
				        	Ни в коем случае не засыпайте при использовании полосок - да, у нас уже были такие случаи.
				        	<br>  
				        	Время ожидания указано на коробке либо на странице с <a href="<?php echo get_post(381)->uri; ?>">таблицей типов полосок 3D Crest White</a> 
				        </p>
				    </div>
				</div>

			    <div class="number-bubble">6</div> 

			</div> 
			<!--end wrap-->  
			<!--start wrap-->
			<div class="wrap">
				<div class="instruction-box">
				    <div class="instruction-crop">
				        <img alt="Избавьтесь от лишнего геля отбеливающих полосок" src="<?php echo get_template_directory_uri(); ?>/public/img/instructions/670px-Use-Teeth-Whitening-Strips-Step-9.jpg" >
				    </div> 
				    <div class="text">
				        <h2>
				        	Избавьтесь от лишнего геля
						</h2> 
				        <p> 
				        	Многие отбеливающие полоски содержат химикаты которые могут быть небезопасны в больших количествах.
				        	<br>
				        	Что не означает что пользоваться полосками опасно для жизни, если вы не решите слизывать гель с 20 полосок каждый день.
				        	<br> 
				        	Рекомендуется аккуратно почистить зубы чтобы избавиться от лишнего геля после использование полосок.
				        </p>
				    </div>

				</div> 
			    <div class="number-bubble">8</div>
			</div> 
			<!--end wrap--> 

		</div>
	</section>
</main> 
<?php get_footer(); ?>

