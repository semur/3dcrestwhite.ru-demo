<?php 
get_header(); 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;
global $product;
?>   

<?php
	$glamorous    = get_post(248);
	$flexfit      = get_post(245);
	$professional = get_post(239);
	$hrexpress    = get_post(277);
	$sensistop    = get_post(285);
	$vivid    	  = get_post(552);

	$hp = new helpers(); 
	$pages = array($sensistop,$vivid,$hrexpress,$glamorous,$professional,$flexfit);  
?> 

<main class="compare-box whitestrips" ng-controller="compareBoxController" ng-click="navigationTrigger(-1)">

	<div class="cap">
	    <div class="custom-shortcode" data-effect="left">
	        <div class="wi-center-heading has_boder">
	            <h1 class="h">Какие полоски подходят твоим зубам?</h1>
	            <h2 class="subtitle">Мы поможем ответить на этот вопрос! У каждого свой уровень чувствительности зубов, а значит каждому подходят разные типы полосок</h2>
	        </div>
	        <div class="viewport-ele"></div>
	    </div> 
 
		<div class="bottom clear">
			<div class="left"> 
				<div class="placeholder"> 
				</div>  
				<?php foreach (get_field("product_compare_settings_w","options") as $key => $value) : ?>
					<div class="name">
						<span>
							<?php echo $value["product_compare_settings_attribute"]; ?>
						</span>
					</div>   
				<?php endforeach; ?>
			</div>
			<div class="right">  
				<div class="wrap" id="tab-wrap"> 
				
					<!-- sensistop -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(0,'<?php echo get_field("product_short_title",$sensistop->ID); ?>')" ng-class="{active : compareBoxIsVisible(0)}">
							<div class="product">
								<?php echo get_the_post_thumbnail ($sensistop->ID) ?>
								<h3><?php echo get_field("product_short_title",$sensistop->ID); ?></h3>
							</div>   
							<span></span>
							<?php $hp->do_compare($sensistop->ID); ?> 
						</div>
					</div>  

					<!-- vivid -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(1,'<?php echo get_field("product_short_title",$vivid->ID); ?>')" ng-class="{active : compareBoxIsVisible(1)}">
							<div class="product">
								<?php echo get_the_post_thumbnail ($vivid->ID) ?>
								<h3><?php echo get_field("product_short_title",$vivid->ID); ?></h3>
							</div>   
							<span></span>
							<?php $hp->do_compare($vivid->ID); ?> 
						</div>
					</div>   

					<!-- 1hr express -->
					<div class="content">
						<div class="shadow" ng-click="compareBoxShow(2,'<?php echo get_field("product_short_title",$hrexpress->ID); ?>')" ng-class="{active : compareBoxIsVisible(2)}">  
							<div class="product">
								<?php echo get_the_post_thumbnail ($hrexpress->ID) ?>
								<h3><?php echo get_field("product_short_title",$hrexpress->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($hrexpress->ID); ?> 
						</div>
					</div>  

					<!-- glamorous -->
					<div class="content">
						<div class="shadow" ng-click="compareBoxShow(3,'<?php echo get_field("product_short_title",$glamorous->ID); ?>')" ng-class="{active : compareBoxIsVisible(3)}"> 
							<div class="product">
								<?php echo get_the_post_thumbnail ($glamorous->ID) ?>
								<h3><?php echo get_field("product_short_title",$glamorous->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($glamorous->ID); ?> 
						</div> 
					</div>  

					<!-- professional -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(4,'<?php echo get_field("product_short_title",$professional->ID); ?>')" ng-class="{active : compareBoxIsVisible(4)}">
							<div class="product">
								<?php echo get_the_post_thumbnail ($professional->ID) ?>
								<h3><?php echo get_field("product_short_title",$professional->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($professional->ID); ?>  
						</div>
					</div>  

					<!-- supreme -->
					<div class="content">
						<div class="shadow" ng-click="compareBoxShow(5,'<?php echo get_field("product_short_title",$flexfit->ID); ?>')" ng-class="{active : compareBoxIsVisible(5)}">
							<div class="product">
								<?php echo get_the_post_thumbnail ($flexfit->ID) ?>
								<h3><?php echo get_field("product_short_title",$flexfit->ID); ?></h3>
							</div>  
							<span></span> 
							<?php $hp->do_compare($flexfit->ID); ?> 
						</div>
					</div>   

				</div>    
			</div> 
		</div>

		<div class="tabs">
			<?php foreach ($pages as $key => $value) : ?>
				<article class="product-box clear" ng-class="{active : compareBoxIsVisible(<?php echo $key; ?>)}">
					<div class="left">
					</div>
					<div class="right">
						<div class="wrap">
						
							<div class="img">
								<?php echo get_the_post_thumbnail ($value->ID) ?>
							</div>
							<div class="text">

								<h2><?php echo get_the_title ($value->ID) ?></h2>
								<p itemprop="description">
									<?php echo get_the_excerpt($value->ID) ?>
								</p>  
								<a href="<?php echo get_the_permalink ($value->ID) ?>" class="submit">
									<span>перейти на страницу товара</span> 
									<span>→</span>
								</a>
							</div> 
						</div> 
					</div> 
				</article>
			<?php endforeach; ?>
		</div>
	</div>
</main>
 



<?php get_footer(); ?>

