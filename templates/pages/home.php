<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package web2feel
 */ 
get_header();
?>
<main id="content" class="content home" ng-click="navigationTrigger(-1)"> 
	  
	<?php include get_template_directory().'/templates/forms/pick-form.php'; ?>
	 
	<?php //include get_template_directory().'/templates/other/who-we-are.php'; ?>
	 
	<section class="products" ng-controller="loopController" data-horizontal-scroll="desktop"> 
		<div class="woocommerce">
			<div class="js-masonry clear" id="js-masonry">
		 	<?php
		 	   
			$args = array(  
			    'post_type'  	 => 'product',    
                'posts_per_page' => -1,
                'orderby'        => 'menu_order' 
			);  
			  
			$query = new WP_Query( $args );  
			      
			if ($query->have_posts()) : 
			while ($query->have_posts()) : $query->the_post();  
				wc_get_template_part( 'content', 'product' );   
		    endwhile;   
			endif;  
			  
			wp_reset_query();  
		 	?>
			</div>
		</div> 
	</section>

	<section class="horizontal-scroll-wrap" ng-controller="loopController" data-horizontal-scroll="mobile"> 

		<h2>Каталог</h2>

		<div class="clear">
			<div class="sly-nav prev">
				<i class="fa fa-chevron-left" aria-hidden="true"></i>
				<span class="text">Предыдущий</span>
			</div>
			<div class="sly-nav next">
				<span class="text">Следующий</span>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</div>
		</div>

		<div class="scrollbar">
			<div class="handle">
				<div class="mousearea"></div>
			</div>
		</div>

		<div class="frame" id="horizontal-products">
			<div class="clearfix"> 
				<?php 
				$args = array(  
				    'post_type'  	 => 'product',    
	                'posts_per_page' => -1,
	                'orderby'        => 'menu_order' , 
				);  
				  
				$query = new WP_Query( $args );  
				      
				if ($query->have_posts()) : 
					while ($query->have_posts()) : $query->the_post();  
						wc_get_template_part( 'content', 'product' );   
				    endwhile;   
				    wp_reset_postdata();
				endif;    
			 	?> 
			</div>
		</div> 
	</section> 

	<?php include get_template_directory().'/templates/instagram/archive-reviews.php'; ?>

	<?php include get_template_directory().'/templates/other/home-tiles.php'; ?>
 	
</main>
<?php get_footer(); ?>