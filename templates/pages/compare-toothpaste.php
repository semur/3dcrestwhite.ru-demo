<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); 
global $post;
global $product;
?>    

<?php
	$radiant = get_post(281);
	$glamorous = get_post(267);
	$luxe = get_post(283);
	$briliance = get_post(289); 
 
	$hp = new helpers(); 
	$pages = array($glamorous,$radiant,$luxe,$briliance); 
?> 

<main class="compare-box toothpaste" ng-controller="compareBoxController" ng-click="navigationTrigger(-1)">

	<div class="cap">
	    <div class="custom-shortcode" data-effect="left">
	        <div class="wi-center-heading has_boder">
	            <h1 class="h">Какие полоски подходят твоим зубам?</h1>
	            <h2 class="subtitle">Мы поможем ответить на этот вопрос! У каждого свой уровень чувствительности зубов, а значит каждому подходят разные типы полосок</h2>
	        </div>
	        <div class="viewport-ele"></div>
	    </div> 
 
		<div class="bottom clear">
			<div class="left"> 
				<div class="placeholder"> 
				</div>  
				<?php foreach (get_field("product_compare_settings_t","options") as $key => $value) : ?>
					<div class="name">
						<span>
							<?php echo $value["product_compare_settings_attribute"]; ?>
						</span>
					</div>   
				<?php endforeach; ?>
			</div>
			<div class="right compare-toothpaste"> 
				<div class="wrap" id="tab-wrap"> 
				
					<!-- radiant -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(0,'<?php echo get_field("product_short_title",$radiant->ID); ?>')" ng-class="{active : compareBoxIsVisible(0)}"> 
							<div class="product" >
								<?php echo get_the_post_thumbnail ($radiant->ID) ?>
								<h3><?php echo get_field("product_short_title",$radiant->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($radiant->ID); ?>  
						</div>
					</div>   

					<!-- glamorous -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(1,'<?php echo get_field("product_short_title",$glamorous->ID); ?>')" ng-class="{active : compareBoxIsVisible(1)}">  
							<div class="product">
								<?php echo get_the_post_thumbnail ($glamorous->ID) ?>
								<h3><?php echo get_field("product_short_title",$glamorous->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($glamorous->ID); ?>  
						</div> 
					</div>  

					<!-- luxe -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(2,'<?php echo get_field("product_short_title",$luxe->ID); ?>')" ng-class="{active : compareBoxIsVisible(2)}"> 
							<div class="product">
								<?php echo get_the_post_thumbnail ($luxe->ID) ?>
								<h3><?php echo get_field("product_short_title",$luxe->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($luxe->ID); ?>   
						</div>
					</div>  

					<!-- brilliance -->
					<div class="content"> 
						<div class="shadow" ng-click="compareBoxShow(3,'<?php echo get_field("product_short_title",$briliance->ID); ?>')" ng-class="{active : compareBoxIsVisible(3)}"> 
							<div class="product">
								<?php echo get_the_post_thumbnail ($briliance->ID) ?>
								<h3><?php echo get_field("product_short_title",$briliance->ID); ?></h3>
							</div>  
							<span></span>
							<?php $hp->do_compare($briliance->ID); ?>  
						</div> 
					</div>  
   
				</div>    
			</div> 
		</div>

		<div class="tabs">
			<?php foreach ($pages as $key => $value) : ?>
				<article class="product-box clear" ng-class="{active : compareBoxIsVisible(<?php echo $key; ?>)}">
					<div class="left">
					</div>
					<div class="right">
						<div class="wrap">
						
							<div class="img">
								<?php echo get_the_post_thumbnail ($value->ID) ?>
							</div>
							<div class="text">

								<h2><?php echo get_the_title ($value->ID) ?></h2>
								<p itemprop="description">
									<?php echo get_the_excerpt($value->ID) ?>
								</p>  
								<a href="<?php echo get_the_permalink ($value->ID) ?>" class="submit">
									<span>перейти на страницу товара</span> 
									<span>→</span>
								</a>
							</div> 
						</div> 
					</div> 
				</article>
			<?php endforeach; ?>
		</div>
	</div>
</main> 
<?php get_footer(); ?>

