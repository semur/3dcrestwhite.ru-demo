<aside class="sidebar"  id="sidebar" ng-cloak>
    <section class="wrap">
        <h2>
            полезные ссылки
            <span class="arrow"></span>
        </h2> 

        <ul class="menu">
        <?php 
            $menu = wp_nav_menu(array(
                'menu'            => 'Sidebar',
                'container'       => false,
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true, 
                'before'          => '',
                'after'           => '', 
                'items_wrap'      => '%3$s',
                'link_before'     => '<span>',
                'link_after'      => '</span>',
                'depth'           => 0,
                'walker'          => new NoClassWalker
            ));  
        ?>
        </ul>
    </section>
</aside>
