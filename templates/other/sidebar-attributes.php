<?php
$hp = new helpers();
$values = $hp->get_compare(get_the_ID(),"check");

$category = get_the_terms( get_the_ID(),'product_cat')[0]->term_id;
$link = "";
$text = "";

if($category==7) {
	$attributes = get_field("product_compare_settings_t","options");
	$link = get_permalink(611);
	$text = "Сравнить зубную пасту";
}else if($category==8){
	$attributes = get_field("product_compare_settings_w","options"); 
	$link = get_permalink(609);
	$text = "Сравнить отбеливающие полоски";
}
?>

<?php if(count($attributes) > 0 && count($values) > 0) : ?>
<div class="wrap attributes" ng-hide="!true" ng-cloak> 

	<h2>Сравнить <?php echo get_field("product_short_title"); ?></h2>
	
	<?php $index = 0;foreach ($attributes as $key => $value) : ?>
		<div class="clear">
			<span class="left">
				<?php echo $values[$index]; ?>
			</span>
			<span class="right">
				<?php echo $value["product_compare_settings_attribute"]; ?>
			</span>
		</div>
	<?php $index++; endforeach; ?>

	<a href="<?php echo $link; ?>" class="submit">
		<?php echo $text; ?>
	</a>

</div>
<?php endif; ?>