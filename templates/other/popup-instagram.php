<!-- instagram feed start -->
<div class="overlay-instagram" ng-controller="popupInstagramController" ng-hide="popupInstagramRemoved" ng-cloak>

	<button class="button" ng-class="{shaker : shake()}" ng-click="popupInstagramShow()">
		Наши отзывы на Instagram
	</button>

	<div class="instagram-feed popup" ng-show="popupInstagramVisible">
		
		<div class="top clear">
			<span class="text">Отзывы наших клиентов</span>
			<span class="close" ng-click="popupInstagramClose()">X</span>
			<span class="fold" ng-click="popupInstagramFold()">-</span> 
		</div>
		<div class="middle" id="instagram-scroller">

			<div ng-show="popupInstagramVisibleLoader"> 
				<!-- loader start -->
				<div class="cs-loader loading" ng-hide="formIsLoaded()">
					<div class="cs-loader-inner">
						<label>	●</label>
						<label>	●</label>
						<label>	●</label>
						<label>	●</label>
						<label>	●</label>
						<label>	●</label>
					</div>
				</div> 
				<!-- loader end -->
			</div>
			<div class="wrap" ng-repeat="review in instagramFeedStatic" ng-hide="popupInstagramVisibleLoader"> 
				<div class="left">
					<div class="circle crop">
						<img ng-src="{{review.img.src}}" alt="{{review.img.alt}}" title="{{review.img.alt}}">
					</div>
				</div>
				<div class="right">
					<span>{{review.username}}</span>
					<p ng-bind-html="review.excerpt"></p>
					<a class="form-link" href="{{review.url}}" target="_blank">Перейти на комментарий на Instagram</a>
				</div> 
			</div>
						  
		</div>
		<div class="bottom">
			<a class="button-link clear" href="<?php echo get_field("global_instagram_link","options"); ?>" target="_blank">
				<span class="text">Подпишись для скидок и промо акции</span>
				<f class="fa fa-instagram"></f>
			</a> 
		</div>
	</div>		
</div>
<!-- instagram feed end -->