<section class="cap who-we-are">
	<div>
		<h2 class="title">
			Немного о нас
		</h2>
		<span class="underline"></span>
		<p class="content">
			Мы - начинающий интернет магазин и официальный представитель продукции Crest-White.
			Мы предлагаем профессиональное отбеливание зубов которое сэкономит вам время и нервы на визитах к врачу!
			Чтобы узнать больше, загляни на <a href="<?php echo get_field("global_instagram_link","options"); ?>">нашу страницу в Instagram</a> 
		</p>
	</div>
</section>