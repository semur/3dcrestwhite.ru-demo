<section class="front-bullets">
	
	<div class="cap">

		<h2>ваше доверие это наша обязанность</h2>

		<div class="wrap">
			<?php
			$arr = array(
				'award'=>array('Только лучшее качество','Продукция обеспечивает качественный и заметный результат уже в первые дни использования'),
				'card'=>array('Оплата при получении','Наш курьер доставить ваш заказ к вам на порог'),
				'clock'=>array('Сэкономь время','Мы поможем вам сэкономить время на визитах к врачу')
			);
			foreach ($arr as $key => $value) : ?>

				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/public/img/bullets/<?php echo $key; ?>.png" width="40" height="40">
					<h4><?php echo $value[0]; ?></h4>
					<p><?php echo $value[1]; ?></p>
				</div> 
				
			<?php endforeach; ?>
			
		</div>
		<div class="wrap">
			<?php
  
			$arr = array( 
				'message'=>array('Коммуникабельность','Мы также можем ответить на любые твои вопросы по E-mail. <a href="'.get_the_permalink(get_post(60)->ID).'">Напиши нам!</a>'),
				'relax'=>array('Помощь','Пиши нам с любыми вопросами о применении и ценах на нашей группе <a href="'.get_field("global_instagram_link","options").'">Instagram</a> или на <a href="'.get_field("global_phone","options").'">Whatsapp</a>'),
				'shield'=>array('Безопасно и эффективно','Мы тестируем всю нашу продукцию прежде чем представить ее вам')
			);
			foreach ($arr as $key => $value) : ?>

				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/public/img/bullets/<?php echo $key; ?>.png" width="40" height="40">
					<h4><?php echo $value[0]; ?></h4>
					<p><?php echo $value[1]; ?></p>
				</div> 
				
			<?php endforeach; ?>
			
		</div>

		<a class="submit" href="<?php echo get_field("global_instagram_link","options"); ?>">
			подпишись на нас для промо акции
		</a>
	</div>

</section>