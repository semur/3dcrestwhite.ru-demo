<section class="front-bullets"> 
	<div class="cap"> 
		<div class="wrap">
			<?php
			$arr = array( 
				'card'=>array('Оплата при получении','Наш курьер доставит ваш заказ к вам на порог'),
				'shield'=>array('Безопасно и эффективно','Мы тестируем всю нашу продукцию прежде чем представить ее вам'),
				'clock'=>array('Сэкономь время','Мы поможем вам сэкономить время на визитах к врачу')
			);
			foreach ($arr as $key => $value) : ?>

				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/public/img/bullets/<?php echo $key; ?>.png" width="40" height="40">
					<h4><?php echo $value[0]; ?></h4>
					<p><?php echo $value[1]; ?></p>
				</div> 
				
			<?php endforeach; ?>
		</div> 
	</div> 
</section>