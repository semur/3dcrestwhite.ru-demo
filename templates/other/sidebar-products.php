<?php /*
<div class="wrap products"> 
	products injected based on recent history, current page, what's in the cart

	flow:
		if has cart products
			if NONSENSITIVE
				offer 5min touchups and sensistop
			else
				offer 5min touchups and toothpaste

		if first page viewed
			show cheapest products - VIVID, 1hr, 5min touchups

		if more than 2 pages viewed or time spent on site above 1min
			show more expensive ones like flexfit, professional, glamorous

</div>
*/ ?>

<?php	 	
global $post; 
$tags = wp_get_post_tags($post->ID);
$q = array();
foreach ($tags as $key => $value) {
	$q[] = $value->name;
} 
$args = array(  
    'post_type'  	 => 'product',    
    'posts_per_page' => $longContent ? 3 : 1, 
    'orderby' 		 => '_sale_price _regular_price',
    'order' 		 => 'DESC',
    's' 			 => $q,
);  
$query = new WP_Query($args);  
if ($query->have_posts()) : 
	while ($query->have_posts()) : $query->the_post();  
		 
		$regular_price = get_post_meta( get_the_ID(), '_regular_price');
		$sale_price = get_post_meta( get_the_ID(), '_sale_price');

		$image_main_medium = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); 
		$image_main_medium = $image_main_medium[0];
?>
	<a href="<?php the_permalink(); ?>" class="wrap product">
		<img src="<?php echo $image_main_medium; ?>" alt="<?php echo $title; ?>"> 

		<h3 class="title">
			<?php echo get_field("product_short_title"); ?> 
		</h3>

		<?php $purpose = get_field("product_purpose"); if($purpose!=""):?>
		<h4 class="purpose" <?php echo $bg; ?>>
			<?php echo $purpose; ?>  
		</h4>		 
		<?php endif; ?>
		 
		<p class="price clear"> 
			<?php if($sale_price[0] != '') : $final = $sale_price[0]; ?>
				<span class="after">
					<span class="number"><?php echo $sale_price[0]; ?> </span>
					<span class="currency">₽</span>
				</span> 
				<span class="before">
					<span class="number"><?php echo $regular_price[0]; ?> </span>
					<span class="currency">руб.</span> 
				</span>
			<?php else: $final = $regular_price[0];  ?>
				<span class="after">
					<span class="number"><?php echo $regular_price[0]; ?> </span>
					<span class="currency">руб.</span>  
				</span>   
			<?php endif; ?>
		</p>
	</a>
	
<?php	 
	endwhile; 
endif;   
wp_reset_query();  
?>