<div class="wrap generic" ng-hide="true" ng-cloak> 
	<?php
	$arr = array( 
		'card'=>array('Оплата при получении','Наш курьер доставить ваш заказ к вам на порог'),
		'shield'=>array('Безопасно и эффективно','Мы тестируем всю нашу продукцию прежде чем представить ее вам'),
		'clock'=>array('Сэкономь время','Мы поможем вам сэкономить время на визитах к врачу')
	);
	foreach ($arr as $key => $value) : ?>

		<a href="<?php echo get_field("global_instagram_link","options"); ?>" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/public/img/bullets/<?php echo $key; ?>.png">
			<h4><?php echo $value[0]; ?></h4>
			<p><?php echo $value[1]; ?></p>
		</a> 
		
	<?php endforeach; ?> 
</div>