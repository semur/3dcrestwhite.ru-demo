<?php
/** 
 * @package web2feel
 */ 
get_header(); 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;


$query = new WP_Query(array(
    'post_type' => 'faq-posts', 
    'posts_per_page' => -1,
)); 
$helpers = new helpers();

$arr = array(); 	
if ($query->have_posts()) {
		  
    while ($query->have_posts()) {  
    	$query->the_post(); 

		$post = get_post();
    	$title = $post->post_title; 
    	$content = apply_filters('the_content', $post->post_content);
    	  
    	$cats = array();
    	foreach (get_the_category() as $key => $value) {
    		$cats[] = $value->name;
    	}
    	$arr[] = array(
    		"title" => $title,
    		"content" => $content,
    		"tags" => $cats,
    	);
    }
} 
$arr = json_encode($arr);  
  
$arrCategories = array(); 
foreach (get_categories() as $key => $value) {

	$arrCategories[$value->name] = array(
		"name" => $value->name, 
		"selected" => 0, 
	);
}
$arrCategories = json_encode($arrCategories);
?> 

<main class="faq" ng-controller="faqController" ng-click="navigationTrigger(-1)"> 
 
	<script>  
		faqs = '<?php echo $arr; ?>';	
		faqs = faqs.replace(/(?:\r\n|\r|\t|\n)/g, ' '); 
		faqs = JSON.parse(faqs);
		settings.faqs = faqs;

		faqsCategories = '<?php echo $arrCategories; ?>';	
		faqsCategories = faqsCategories.replace(/(?:\r\n|\r|\t|\n)/g, ' '); 
		faqsCategories = JSON.parse(faqsCategories);
		settings.faqsCategories = faqsCategories;
	</script>
  
	<section class="header cap">
	    <div class="left">
	        <div class="heading clear"> 
	            <div class="right">
	                <h1>
					    <span class="big"><?php the_title(); ?></span>  
					</h1> 
	            </div>
	        </div> 
	    </div>
	    <div class="right">
			<?php echo get_sidebar(); ?> 
	    </div>
	</section>
  
	<section class="posts"> 

        <div class="wrapper clear">
        	<div class="content"> 

	        	<div class="filters clear" ng-cloak>
	        		<span class="filter">Отфильтровать по тегам</span>
			  		<button class="button" ng-click="removeFaqsFilter()" ng-class="faqIsInactive()">
			  			<span>Все ЧаВО</span>
			  		</button>

			  		<button class="button" ng-repeat="faq in getFaqsCategories()" ng-click="addFaqsFilter(faq.name)" ng-class="faqIsActive(faq.name)">
			  			<span>{{faq.name}}</span>
			  		</button>
			  	</div>

		    	<article ng-repeat="faq in getFaqs() | filter:filterByGenres" ng-cloak> 
		    		<h3 class="title">{{faq.title}}</h3>
		    		<div class="content" ng-bind-html="faq.content"></div>
		    	</article>
			</div>
        	<div class="sidebar-tiles" id="stick-target">   
				<?php include get_template_directory().'/templates/other/sidebar-tiles-celebrities.php'; ?>
				<?php include get_template_directory().'/templates/other/sidebar-attributes.php'; ?>
        	</div>
        </div>
	</section>
</main> 
<?php get_footer(); ?> 