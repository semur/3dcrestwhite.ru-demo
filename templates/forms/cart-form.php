<!-- cart form start -->
<section class="form">
	 
	<div class="form-wrap">
		<div id="cart-contents">
			<?php the_content(); ?>
		</div>
		<div class="city-selection clear hidden">
			<div class="cart-button" ng-class="{active : cityActive(0)}" ng-click="cityChange(0)">
				<span>доставка в москву</span>
			</div>
			<div class="cart-button" ng-class="{active : cityActive(1)}" ng-click="cityChange(1)">
				<span>доставка за пределы москвы</span>
			</div>
		</div>
		<div class="tabs" ng-hide="cartIsEmpty()" ng-cloak>
			<div class="tab" ng-show="cityActive(0)">
				<div class="links hidden">
					<span class="link" ng-class="{active : paymentMethodActive(0)}" ng-click="paymentMethodChange(0)">
						оплата после доставки
					</span>
					<span class="link" ng-class="{active : paymentMethodActive(1)}" ng-click="paymentMethodChange(1)">
						PayPal
					</span> 
				</div>
				<div class="contents">
					<div class="content" ng-show="paymentMethodActive(0)" id="checkout-form-email"> 
 
						<!-- email checkout start -->
						<form class="email-form" name="formCheckout" ng-submit="checkoutFormEmail(customer)" ng-disabled="form.$invalid" ng-hide="checkoutSubmitted">
							<input type="text" name="name" ng-model="customer.name" placeholder="Имя *" >
							<span class="error name">Нужно заполнить это поле</span>
							<input type="email" name="email" ng-model="customer.email" placeholder="Email *" >
							<span class="error email">Нужно заполнить это поле</span>
							<input type="text" name="phone" ng-model="customer.phone" placeholder="Номер телефона *" > 
							<span class="error phone">Нужно заполнить это поле</span>
							<textarea rows="10" name="address" ng-model="customer.address" placeholder="Адрес"></textarea>
							<?php /* ?><button class="submit" ng-class="{active: cartIsLoading}"> 
								<span button-text="Оформить заказ" 3d-button></span> 
							</button> <?php */ ?>

							<button class="submit" ng-class="{active: cartIsLoading}"> 
								<span button-text="Оформить заказ">
									<span class="text ng-binding">Оформить заказ</span>
									<div class="cs-loader loading">
										<div class="cs-loader-inner">
											<label> ●</label>
											<label> ●</label>
											<label> ●</label>
											<label> ●</label>
											<label> ●</label>
											<label> ●</label>
										</div>
									</div> 
								</span> 
							</button>
						</form>  
						<!-- email checkout end -->

					</div>
					<div class="content hidden" ng-show="paymentMethodActive(1)">
						
					</div> 
				</div>
			</div>
			<div class="tab hidden" ng-show="cityActive(1)">
				<div class="links">
					<span class="link" ng-class="{active : paymentMethodActive(0)}" ng-click="paymentMethodChange(0)">PayPal</span>
					<span class="link" ng-class="{active : paymentMethodActive(1)}" ng-click="paymentMethodChange(1)">RoboKassa</span>
				</div>
				<div class="contents"> 
					<div class="content" ng-show="paymentMethodActive(0)">
						
					</div> 
					<div class="content" ng-show="paymentMethodActive(1)">
						
					</div> 
				</div>
			</div>
		</div>

	</div>
</section> 
<!-- cart form end -->

 