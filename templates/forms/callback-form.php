<span class="text" ng-hide="callbackSubmitted">Заказать звонок - мы вам перезвоним</span>
<form class="form" name="form" ng-submit="addToCallbacks(customer)" ng-disabled="form.$invalid" ng-hide="callbackSubmitted">
	<input type="text" name="phone" ng-model="customer.phone" placeholder="Номер телефона" required>
	<button ng-show="form.phone.$valid" ng-click="addSpinner($event)">
		<span class="checkmark"></span>
	</button> 
</form> 
<div class="text" ng-hide="!callbackSubmitted">
	<i class="fa fa-smile-o" aria-hidden="true"></i>
	мы с вами свяжемся!
</div>