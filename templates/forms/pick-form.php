<!-- pick form start -->
<section class="pick-form" id="pick-form" ng-controller="pickFormController">

    <div class="overlay"></div> 

    <!-- insert the appropriate image based on device size -->  
	<div ng-bind-html="showPickFormImage()"></div>

	<div class="wrap" id="form-box">

		<h1 class="title">
			профессиональное отбеливание зубов с доставкой на дом
		</h1>

		<h2 class="subtitle"> 
			выбери свой тип полосок, сравни цены, и назначь время доставки твоей новой улыбки
		</h2>  
 
		<form class="form" id="form-pick-treatment">
			<div class="left"> 
				<div class="section options-3">
					 
					<div class="button" ng-click="outerTabChange(0)" ng-class="{active: outerTabActive(0)}">
						<div class="box">
							<span>быстрый курс отбеливания</span>
						</div>
						<label>для неприхотливых зубов</label>
					</div>
					<div class="button" ng-click="outerTabChange(1)" ng-class="{active: outerTabActive(1)}"> 
						<div class="box">
							<span>наиболее комфортный курс</span>
						</div>
						<label>
							для чувствительных зубов
						</label>
					</div> 
					<div class="button" ng-click="outerTabChange(2)" ng-class="{active: outerTabActive(2)}"> 
						<div class="box">
							<span>курс с долгим результатом</span>
						</div>
						<label>
							для тех кто не торопится
						</label>
					</div>  

				</div>
				
				<?php /*
				<div class="section options-2"> 
					<div class="button checkmark"> 
						<div class="box">
							<span>ускоренный курс</span>
						</div>
						<label>добавить 1hr-Express если результат нужен уже сегодня/завтра</label>
					</div>
					<div class="button checkmark"> 
						<div class="box">
							<span>сделать скидку</span>
						</div>
						<label>
							мы заплатим 300₽ за пост результатов лечения в Instagram
						</label>
					</div>  
				</div>
				*/ ?>

				<div class="section options-2"> 
					<div class="button checkmark" ng-click="changeUpsell(0)" ng-class="{active: upsellActive(0)}">
						<div class="box">
							<span>закрепить результат</span>
						</div> 
						<label>добавить полоски для ежедневного использования</label>
					</div>
					<div class="button checkmark" ng-click="changeUpsell(1)" ng-class="{active: upsellActive(1)}"> 
						<div class="box">
							<span>чувствительные зубы</span>
						</div> 
						<label>добавить полоски для снижения чувствительности зубов</label>
					</div>  
				</div> 
    			  
				<div>
					<button class="submit" ng-click="addToCartPickForm($event)" ng-class="{active: cartIsLoading}"> 
				 		<span  button-text="добавить в корзину" 3d-button></span> 
					</button>
				</div>

			</div>
			<div class="right"> 
				<div class="side-wrap">
					<!-- loader start -->
					<div class="cs-loader loading" ng-hide="formIsLoaded()">
						<div class="cs-loader-inner">
							<label>	●</label>
							<label>	●</label>
							<label>	●</label>
							<label>	●</label>
							<label>	●</label>
							<label>	●</label>
						</div>
					</div> 
					<!-- loader end -->
	
					<!-- tab group start -->
				 	<div class="tab-group" ng-repeat="outerTab in outerTabs" ng-class="{active: outerTabActive($index)}" ng-show="formIsLoaded()" ng-cloak>
				   
						<div class="tab-links"> 
							<span class="tab-link" ng-repeat="link in outerTab" ng-class="{active: innerTabActive($index)}" ng-click="innerTabChange($index)">{{ link.short_title }}</span>
						</div>
						<div class="tab-contents">

							<div class="scroll-wrap">

								<div class="tab-content" ng-repeat="product in outerTab" ng-class="{active: innerTabActive($index)}">
									<h2 class="side-subtitle">{{ product.category.name }} {{ product.short_title }}</h2>
									<span class="price">
										<span class="number">{{ product.price }}</span>
										<span class="currency">₽</span>
									</span>
									<span class="form-attribute quantity-discounts">
										<div ng-repeat="attribute in product.attributes">
										    <span>{{ attribute.key }}</span>
										    <span>{{ attribute.value }}</span> 
										</div>
									</span>
									<a class="link" href="{{ product.link }}" target="_blank">перейти на страницу товара</a>
								</div> 

								<!-- upsells start --> 
								<div class="tab-content upsell" ng-class="{active: upsellActive(0)}">
									<div class="close" ng-click="changeUpsell(0)">X</div>
									<h2 class="side-subtitle">{{ outerTabs.fortify.category.name }} {{ outerTabs.fortify.short_title }}</h2>
									<span class="price">
										<span class="number">{{ outerTabs.fortify.price }}</span>
										<span class="currency">₽</span>
									</span>
									<span class="form-attribute quantity-discounts">
										<div>
										    <p>
										    Для поддержания и закрепления результата спустя несколько месяцев после окончания курса отбеливания. В упаковке 56 полоскок, использовать 1 раз в день, оставляя их на зубах на 5 минут
										    </p>
										</div>
									</span> 
									<a class="link" href="{{ outerTabs.fortify.link }}" target="_blank">перейти на страницу товара</a>
								</div>  
								<div class="tab-content upsell" ng-class="{active: upsellActive(1)}">
									<div class="close" ng-click="changeUpsell(1)">X</div>
									<h2 class="side-subtitle">{{ outerTabs.relief.category.name }} {{ outerTabs.relief.short_title }}</h2>
									<span class="price">
										<span class="number">{{ outerTabs.relief.price }}</span>
										<span class="currency">₽</span>
									</span>
									<span class="form-attribute quantity-discounts">
										<div>
										    <p>
										    Рекомендуется для чувствительных зубов или десен, или в сочетании с быстрыми курсами отбеливания
										    </p>
										</div>
									</span> 
									<a class="link" href="{{ outerTabs.relief.link }}" target="_blank">перейти на страницу товара</a>
								</div>    
								<!-- upsells end -->
							</div> 
						</div> 
					</div> 
					<!-- tab group end -->

					<div class="total clear" ng-show="formIsLoaded()" ng-cloak>
						<span class="label">общая стоимость: </span>
						<span class="price">
							<span class="number">{{ getTotalPrice }}</span>
							<span class="currency">₽</span>
						</span>
					</div>
				</div> 
			</div>
		</form>
	</div>
	  
</section> 	 
<!-- pick form end -->

 