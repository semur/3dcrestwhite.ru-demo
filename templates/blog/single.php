<?php
/** 
 * @package web2feel
 */ 
get_header(); 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;
 
$content = get_the_content(); 
$longContent = strlen($content) > 4000;
?> 
<main class="blog-single cap" ng-click="navigationTrigger(-1)"> 
	<div class="image">  
		<img src="<?php helpers::theImage('large'); ?>">
	</div>
	<div class="wrap">
		<div class="content">
			<article class="pull">
		        <h1 class="title">
				    <?php the_title(); ?>  
				</h1> 
				<div class="text">
					<?php echo apply_filters('the_content', $content); ?> 
				</div>  
			</article>

			<div class="archive-navigation">
				<div class="nav prev">
					<?php previous_post_link("%link","%title"); ?>  
				</div>
				<div class="nav next">
					<?php next_post_link("%link","%title"); ?>				
				</div>  
			</div> 
	    </div>
	    <div class="sidebar-tiles" id="stick-target">
			<?php include get_template_directory().'/templates/other/sidebar-products.php'; ?> 
	    </div> 
	</div>
</main> 
<?php get_footer(); ?> 