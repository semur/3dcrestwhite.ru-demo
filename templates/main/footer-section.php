<footer> 
	<div class="cap clear">
		<div class="left">
			<h4>Полезные ссылки</h4>
			<ul class="menu">
			<?php 
				$menu = wp_nav_menu(array(
				    'menu'            => 'Footer Navigation Menu',
				    'container'       => false,
				    'container_class' => '',
				    'container_id'    => '',
				    'menu_class'      => '',
				    'menu_id'         => '',
				    'echo'            => true, 
				    'before'          => '',
				    'after'           => '', 
				    'items_wrap'      => '%3$s',
				    'link_before'     => '<span>',
				    'link_after'      => '</span>',
				    'depth'           => 0,
				    'walker'          => new WalkerAngular
				));  
			?>
			</ul>
		</div>
		<div class="middle">
			<h4>Есть вопросы?</h4>
			<ul class="callback-dropdown menu" ng-controller="callbackController">
				<li class="menu-item callback-call-us clear">
					<a href="tel:<?php echo ot_get_option("global_phone"); ?>">
						<span class="text">Наш телефон:</span>
						<span class="phone"><?php echo ot_get_option("global_phone"); ?></span>
					</a>
				</li>
				<li class="menu-item callback-email-us">
					<a href="<?php echo get_permalink(684); ?>">
						<span>Написать нам E-mail</span> 
					</a>
				</li>
				<li class="menu-item callback-form" ng-cloak>
					<?php include get_template_directory().'/templates/forms/callback-form.php'; ?>   
				</li>
			</ul>
		</div>
		<div class="right">
			<h4>Сутками в офисе по адресу</h4>
			<div class="text">
				<?php echo get_field("global_address","options"); ?>
			</div>
		</div>
	</div>
</footer>  