<?php 
/**
 * The template for displaying all pages.
 * Template name:Blog
 * This is the template that displays the contact us page
 *
 * @package web2feel
 */ 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;
include 'templates/blog/archive.php';