<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package web2feel
 */ 
global $woocommerce; 
global $post;
global $product; 
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>  

	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />

	<meta name="google-site-verification" content="ClmwwI4xTM2OypiF2als5ht3IWHT4JOw5_HMIDUhnYE" />
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">  
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" /> 

	<title><?php wp_title(); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	  
	<?php wp_head(); ?>

	<?php /* ?>
 	<!-- start fast rendering -->
    <noscript id="deferred-styles">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

		<?php if(!WP_LOCAL): ?>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/public/css/min.css"> 
		<?php endif; ?>

    </noscript>
    <script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script> 
    <!-- stop fast rendering -->
	<?php */ ?>
   
</head>
<body <?php body_class("no-fonts"); ?> ng-app="3dApp" ng-controller="mainController">
     
	<?php include get_template_directory().'/templates/navigation/navigation-mobile.php'; ?> 

	<!-- outer wrap start -->
	<div class="shift">
		<?php include get_template_directory().'/templates/main/header-section.php'; ?> 